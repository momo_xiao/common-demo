﻿using Js截屏.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Js截屏.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }
        public ActionResult Screen()
        {
            return View();
        }
    }
}
