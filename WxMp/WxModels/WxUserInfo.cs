﻿namespace WxMp.WxModels
{
    public class WxUserInfo
    {
        /// <summary>
        /// 用户是否订阅该公众号标识
        /// </summary>
        public int subscribe { get; set; }
        public string openid { get; set; }
        public string language { get; set; }
        /// <summary>
        /// 访问时间
        /// </summary>
        public string subscribe_time { get; set; }
        public string unionid { get; set; }
        public string remark { get; set; }
        /// <summary>
        /// 用户分组
        /// </summary>
        public int groupid { get; set; }
        public int[] tagid_list { get; set; }
        public string subscribe_scene { get; set; }
        public string qr_scene { get; set; }
        public string qr_scene_str { get; set; }
    }
}
