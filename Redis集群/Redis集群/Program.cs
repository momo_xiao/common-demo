﻿using CSRedis;
using StackExchange.Redis;
using static CSRedis.CSRedisClient;

namespace Redis集群
{
    internal class Program
    {
        static void Main()
        {
            #region 通过CSRedisClient读取
            /*
            var redis = new CSRedisClient("127.0.0.1:6379");

            //获取节点信息
            NodesServerManagerProvider nodesServerManagerProvider = new NodesServerManagerProvider(redis);

            var nodes = nodesServerManagerProvider.Info(InfoSection.Replication);
            
            Dictionary<string,string> slaveInfo = new Dictionary<string,string>();

            //节点信息存入字典
            if (nodes.Length > 0 )
            {
                var nodeInfo = nodes.FirstOrDefault().value.Split(Environment.NewLine);
                foreach( var node in nodeInfo ) {
                    if (node.Contains(":"))
                    {
                        var slave = node.Split(":");
                        slaveInfo.Add(slave[0], slave[1]);
                    }
                }
            }

            //向主节点写入1条数据
            redis.Set("name", "zhangsan");

            //从节点总数
            var slaveNodeCount = int.Parse(slaveInfo["connected_slaves"]);

            //获取所有从节点IP和端口
            var slaveNodes = slaveInfo.Where(m=>m.Key.StartsWith("slave"))
                .Select(x =>
                {
                    var currNode = x.Value.Split(",");
                    var ip = currNode[0].Split('=')[1];
                    var port = currNode[1].Split('=')[1];
                    return $"{ip}:{port}";
                }).ToArray();

            //测试随机从节点读取数据
            //随机读取10次
            Random random = new Random();
            for (int i = 0; i < 20; i++)
            {
                var randomNode = slaveNodes[random.Next(0, slaveNodeCount)];
                var redis_slave = new CSRedisClient(randomNode);
                Console.WriteLine($"节点：{randomNode},值：{redis_slave.Get("name")}");
            }
            */
            #endregion

            #region 通过StachExchange.Redis获取节点信息
            var options = ConfigurationOptions.Parse("127.0.0.1:6379");
            options.SyncTimeout = int.MaxValue;
            options.AllowAdmin = true;
            // 连接到 Redis 主节点
            var connection = ConnectionMultiplexer.Connect(options);

            // 获取主节点的 INFO replication 信息
            var server = connection.GetServer("127.0.0.1", 6379);
            var info = server.Info("replication");

            Dictionary<string, string> slaveInfo = new Dictionary<string, string>();
            foreach (var item in info)
            {
                Console.WriteLine(item.Key);
                
                foreach (var value in item)
                {
                    Console.WriteLine($"Key:{value.Key},Value:{value.Value}");
                    slaveInfo.Add(value.Key, value.Value);
                }
                Console.WriteLine("------------------------");
            }

            //从节点总数
            var slaveNodeCount = int.Parse(slaveInfo["connected_slaves"]);

            //获取所有从节点IP和端口
            var slaveNodes = slaveInfo.Where(m => m.Key.StartsWith("slave"))
                .Select(x =>
                {
                    var currNode = x.Value.Split(",");
                    var ip = currNode[0].Split('=')[1];
                    var port = currNode[1].Split('=')[1];
                    return $"{ip}:{port}";
                }).ToArray();

            Random random = new Random();
            for (int i = 0; i < 20; i++)
            {
                var randomNode = slaveNodes[random.Next(0, slaveNodeCount)];
                Console.WriteLine($"节点：{randomNode}");
            }
            // 关闭连接
            connection.Close();
            #endregion
        }
    }
}