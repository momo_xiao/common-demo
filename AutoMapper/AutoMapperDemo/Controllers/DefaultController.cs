﻿using Application;
using AutoMapper;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace AutoMapperDemo.Controllers
{
    public class DefaultController : Controller
    {
        private readonly IMapper mapper;

        public DefaultController(IMapper mapper)
        {
            this.mapper = mapper;
        }

        public IActionResult Index()
        {
            Member member = new Member {
                Age = 1,
                Birthday = DateTime.Now,
                MemberId = 10,
                MemberName = "张三"
            };

            MemberDto memberDto = mapper.Map<MemberDto>(member);

            return View();
        }
    }
}
