﻿using System;

namespace 委托示例
{
    public delegate void StateChangedEventHandler(string newState);  //声明事件的委托类型
    internal class Program
    {
        
        static void Main(string[] args)
        {
            StateChangedEventHandler stateChangedEvent;

            A a = new A();
            B b = new B();

            //多播
            stateChangedEvent = a.Method;

            stateChangedEvent += b.Method;

            stateChangedEvent(DateTime.Now.ToShortDateString());

            Console.ReadLine();
        }
    }

    public class A
    {
        public void Method(string method) 
        {
            Console.WriteLine(method);
        }
    }

    public class B
    {
        public void Method(string method)
        {
            Console.WriteLine("当前时间:" + method);
        }
    }
}
