﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 多线程进度条
{
    public partial class Form2 : Form
    {
        Thread thread;
        public Form2()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            HttpClient httpClient = new HttpClient();
            string Html = await httpClient.GetStringAsync("https://www.google.ee");
            Invoke(new Action(() => {
                this.textBox1.Text = Html;
            }));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("消息提示");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox2.Text = Thread.CurrentThread.ThreadState.ToString();
        }
    }
}
