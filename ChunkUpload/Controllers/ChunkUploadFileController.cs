﻿using Microsoft.AspNetCore.Mvc;
using System.Globalization;

namespace ChunkUpload.Controllers
{
    public class ChunkUploadFileController : Controller
    {
        private readonly string savePath = "wwwroot/uploads"; // 假设保存路径

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult FileList()
        {
            return View();
        }

        /// <summary>
        /// 执行上传
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> swfUpload(string uploadtype, int chunk, int chunks, string name, string uuid)
        {
            IFormFile postedFile = Request.Form?.Files["file"];

            //系统临时目录
            string tempFolderPath = Path.Combine(Path.GetTempPath(), uuid);
            if (!Directory.Exists(tempFolderPath))
            {
                Directory.CreateDirectory(tempFolderPath);
            }

            // 检查是否已经上传过该分片
            string newFileName = $"{chunk}_{DateTime.Now.ToString("yyyyMMddHHmmss_ffff", DateTimeFormatInfo.InvariantInfo)}";
            string filePath = Path.Combine(tempFolderPath, newFileName);
            if (System.IO.File.Exists(filePath))
            {
                return Json(new
                {
                    chunkfilename = newFileName,
                    skip = true
                });
            }

            if (postedFile.Length > 0)
            {
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await postedFile.CopyToAsync(stream);
                }
            }

            //说明上传完成，可以合并文件
            if (chunk + 1 == chunks)
            {
                string[] files = Directory.GetFiles(tempFolderPath);
                await MergeFiles(files, Path.GetExtension(name));
            }

            return Json(new
            {
                chunkfilename = newFileName,
                skip = false
            });
        }

        public async Task MergeFiles(string[] chunkFilePaths, string ext, string uploadtype = "image")
        {
            string dirName = string.IsNullOrWhiteSpace(uploadtype) ? "image" : uploadtype;
            string dirPath = Path.Combine(Directory.GetCurrentDirectory(), savePath, dirName);
            //创建文件夹
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }
            string ymd = DateTime.Now.ToString("yyyyMMdd", DateTimeFormatInfo.InvariantInfo);
            dirPath = Path.Combine(dirPath, ymd);
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }

            string newFileName = DateTime.Now.ToString("yyyyMMddHHmmss_ffff", DateTimeFormatInfo.InvariantInfo) + ext;

            using (var targetFileStream = new FileStream(Path.Combine(dirPath, newFileName), FileMode.Create))
            {
                foreach (var chunkFilePath in chunkFilePaths)
                {
                    using (var chunkFileStream = new FileStream(chunkFilePath, FileMode.Open))
                    {
                        await chunkFileStream.CopyToAsync(targetFileStream);
                    }
                }
            }

            // 删除分片文件
            foreach (var chunkFilePath in chunkFilePaths)
            {
                System.IO.File.Delete(chunkFilePath);
            }
        }

        [HttpGet]
        public IActionResult GetFileList(int pageNumber = 1, int pageSize = 10)
        {
            var files = GetFilesRecursively(savePath);
            var pagedFiles = files.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            return Json(new
            {
                Code = 0,
                Msg = "",
                Count = files.Count,
                Data = pagedFiles


                /*Result = "success",
                Data = pagedFiles,
                Message = "获取成功",
                Pager = new
                {
                    RecTotal = files.Count,
                    RecPerPage = pageSize,
                    Page = pageNumber
                }*/
            });
        }

        private List<FileDetails> GetFilesRecursively(string path)
        {
            var files = new List<FileDetails>();
            var dirInfo = new DirectoryInfo(path);

            foreach (var file in dirInfo.GetFiles())
            {
                files.Add(new FileDetails
                {
                    Name = file.Name,
                    Size = file.Length,
                    UploadTime = file.CreationTime
                });
            }

            foreach (var dir in dirInfo.GetDirectories())
            {
                files.AddRange(GetFilesRecursively(dir.FullName));
            }

            return files;
        }

        public class FileDetails
        {
            public string Name { get; set; }
            public long Size { get; set; }
            public DateTime UploadTime { get; set; }
        }
    }
}