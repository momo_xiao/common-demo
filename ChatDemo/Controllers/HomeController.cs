﻿using ChatDemo.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net.WebSockets;
using Newtonsoft.Json;
using System.Text;
using System;
using Microsoft.Extensions.Caching.Distributed;

namespace ChatDemo.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly Dictionary<string, WebSocket> sockets;
        private readonly IDistributedCache cache;
        private byte[] buffer = new byte[1024 * 4];

        public HomeController(
            ILogger<HomeController> logger, 
            Dictionary<string, WebSocket> sockets,
            IDistributedCache cache
            )
        {
            _logger = logger;
            this.sockets = sockets;
            this.cache = cache;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult users()
        {
            List<user> list = new List<user> { 
                new user{ uid = "69b0f33b73d54f45be523de8ddba47d1", name = "张三" },
                new user{ uid = "92b40215da6c4a0aaa98a04d5811a4ce", name = "李四" },
                new user{ uid = "6a08fcd12c42468486cbca856643bc6a", name = "王五" }
            };            
            return Json(list);
        }

        [HttpGet("/chat")]
        public async Task<IActionResult> chat()
        {
            if (Request.Path == "/chat")
            {
                if (HttpContext.WebSockets.IsWebSocketRequest)
                {
                    using var webSocket = await HttpContext.WebSockets.AcceptWebSocketAsync();

                    //接收数据
                    var receiveResult = await webSocket.ReceiveAsync(
                        new ArraySegment<byte>(buffer), CancellationToken.None);

                    //从缓冲区中按接收到的数量，获取接收到的数据
                    var json = Encoding.UTF8.GetString(buffer, 0, receiveResult.Count);

                    user user = JsonConvert.DeserializeObject<user>(json);
                    
                    if(user == null)
                    {
                        throw new Exception("无此用户");
                    }

                    //加入字典
                    sockets.Add(user.uid, webSocket);

                    await Echo(webSocket);
                }
                else
                {
                    HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
                }
            }
            return Ok();
        }

        private async Task Echo(WebSocket webSocket)
        {
            while (webSocket.State == WebSocketState.Open)
            {
                var receiveResult = await webSocket.ReceiveAsync(
                    new ArraySegment<byte>(buffer), CancellationToken.None);

                //获取接收到的消息
                message message = JsonConvert.DeserializeObject<message>(
                    Encoding.UTF8.GetString(buffer, 0, receiveResult.Count)
                    );

                //判断是否存在相关的WebSocket对象
                if(!string.IsNullOrWhiteSpace(message.to) && sockets.ContainsKey(message.to))
                { 
                    var toSocket = sockets[message.to];
                    await toSocket.SendAsync(
                        new ArraySegment<byte>(Encoding.UTF8.GetBytes(message.msg)),
                        WebSocketMessageType.Text,
                        true,
                        CancellationToken.None);
                }
            }
        }
    }
}