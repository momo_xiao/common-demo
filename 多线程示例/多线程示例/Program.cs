﻿using System.Runtime.InteropServices;

namespace 多线程示例
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            // 创建取消标志
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken cancellationToken = cancellationTokenSource.Token;

            //获取一个ID，用于指示当前线程正在哪个处理器上执行。
            Console.WriteLine("主线程处理器ID：" + Thread.GetCurrentProcessorId());

            Thread thread = new Thread(obj =>
            {
                #region 线程调试工具栏中的TID，即[xxxx]里的数字            
                [DllImport("kernel32.dll")]
                static extern uint GetCurrentThreadId();

                // 获取当前线程的 TID
                uint tid = GetCurrentThreadId();

                // 输出 TID
                Console.WriteLine($"当前线程的 TID 为：{tid}");
                #endregion

                //获取一个ID，用于指示当前线程正在哪个处理器上执行。
                Console.WriteLine("子线程处理器ID：" + Thread.GetCurrentProcessorId());

                //当前线程ID
                Console.WriteLine("当前线程ID：" + Thread.CurrentThread.ManagedThreadId);

                //设置后台线程
                Thread.CurrentThread.IsBackground = true;

                //Background
                Console.WriteLine("当前线程状态3:" + Thread.CurrentThread.ThreadState);

                int i = 0;
                while (!cancellationToken.IsCancellationRequested)
                {
                    Console.WriteLine((Guid)obj);
                    Thread.Sleep(1000);
                    i++;

                    if (i == 3)
                    {
                        //发出信号,取消线程执行，不能使用Abort终止，否则会抛出异常
                        cancellationTokenSource.Cancel();
                        break;
                    }
                }
            });
            //线程名称，不可重复设置
            thread.Name = "Test";

            //线程优先级，作用不大，优先级不一定会被操作系统所遵循的
            //因为具体的调度行为取决于操作系统和底层的硬件平台
            thread.Priority = ThreadPriority.Normal;

            //Unstarted
            Console.WriteLine("当前线程状态1:" + thread.ThreadState);

            //开始线程
            thread.Start(Guid.NewGuid());

            //Running
            Console.WriteLine("当前线程状态2:" + thread.ThreadState);

            // 等待线程结束
            thread.Join();

            //Stopped
            Console.WriteLine("当前线程状态4:" + thread.ThreadState);

            Console.ReadLine();
        }
    }
}