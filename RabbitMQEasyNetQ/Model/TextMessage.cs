﻿namespace RabbitMQEasyNetQ.Model
{
    public class TextMessage
    {
        public string Text { get; set; }
    }


    public class MessageDto
    {
        public string Text { get; set; }
    }
}
