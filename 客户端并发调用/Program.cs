﻿using System.Diagnostics;

namespace 客户端并发调用
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var test = new Test();
            Stopwatch stopwatch = new Stopwatch();
            //RedLock可支持300个并发不出错
            Parallel.For(1, 30, async (i) =>
            {
                stopwatch.Restart();
                HttpClient httpClient = new HttpClient();
                var StackExchange = await httpClient.GetStringAsync("https://localhost:7138/api/Home");
                //var RedLock = await httpClient.GetStringAsync("https://localhost:7012/api/Home");
                stopwatch.Stop();
                Console.WriteLine("总耗时：{0},返回结果：{1}", stopwatch.ElapsedMilliseconds, StackExchange);
            });
            Console.ReadKey();
        }
    }

    public class Test
    {
        object o = new object();

        // 有10个商品库存
        private static int stockCount = 1;

        public bool Buy()
        {
            if (stockCount > 0)
            {
                // 模拟执行的逻辑代码花费的时间
                Thread.Sleep(new Random().Next(10, 50));
                stockCount--;
                return true;
            }
            return false;
        }
    }
}
