﻿using System;
using System.Collections.Generic;

namespace 简单观察者模式
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ConcreteSubject subject = new ConcreteSubject();
            //观察者
            ConcreteObserver1 observer1 = new ConcreteObserver1();
            ConcreteObserver2 observer2 = new ConcreteObserver2();

            subject.RegisterObserver(observer1);
            subject.RegisterObserver(observer2);

            Console.WriteLine("通知观察者");
            subject.NotifyObservers();

            subject.UnregisterObserver(observer2);

            Console.WriteLine("再次通知观察者");
            subject.NotifyObservers();

            Console.ReadKey();
        }
    }

    // 观察者接口
    public interface IObserver
    {
        void Update();
    }

    // 具体观察者1
    public class ConcreteObserver1 : IObserver
    {
        public void Update()
        {
            Console.WriteLine("具体观察者1已更新");
        }
    }

    // 具体观察者2
    public class ConcreteObserver2 : IObserver
    {
        public void Update()
        {
            Console.WriteLine("具体观察者2已更新");
        }
    }

    // 发布者
    public interface ISubject
    {
        //注册——绑定
        void RegisterObserver(IObserver observer);
        //移除注册——解绑
        void UnregisterObserver(IObserver observer);
        //通知观察者
        void NotifyObservers();
    }

    // 发布者--实现类
    public class ConcreteSubject : ISubject
    {
        private List<IObserver> observers = new List<IObserver>();

        public void RegisterObserver(IObserver observer)
        {
            observers.Add(observer);
            Console.WriteLine("注册观察者");
        }

        public void UnregisterObserver(IObserver observer)
        {
            observers.Remove(observer);
            Console.WriteLine("删除观察者");
        }

        public void NotifyObservers()
        {
            foreach (IObserver observer in observers)
            {
                observer.Update();
            }
        }
    }

}
