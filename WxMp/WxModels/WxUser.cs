﻿using System.Collections.Generic;

namespace WxMp.WxModels
{
    public class WxUser
    {
        public int total { get; set; }
        public int count { get; set; }
        public openids data { get; set; }
    }

    public class openids
    {
        public string[] openid { get; set; }
    }
}
