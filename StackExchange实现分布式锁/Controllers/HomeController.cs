﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace StackExchange实现分布式锁.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly ILogger<HomeController> logger;
        private readonly RedisLock redisLock;
        private static int stockCount = 10;

        public HomeController(ILogger<HomeController> logger, RedisLock redisLock)
        {
            this.logger = logger;
            this.redisLock = redisLock;
        }

        [HttpGet]
        public async Task<IActionResult> Buy()
        {
            redisLock.Lock();
            if (stockCount <= 0)
            {
                logger.LogInformation("秒杀失败" + stockCount);
                redisLock.UnLock();
                return Ok(new
                {
                    result = false,
                    stockCount = stockCount
                });
            }

            await Task.Delay(new Random().Next(100, 500));
            stockCount--;
            logger.LogInformation("秒杀成功" + stockCount);
            redisLock.UnLock();
            return Ok(new
            {
                result = true,
                stockCount = stockCount
            });

            /*logger.LogInformation($"执行前ID：{Thread.CurrentThread.ManagedThreadId}");
            await Task.Delay(new Random().Next(100, 500)).ConfigureAwait(true);
            logger.LogInformation($"执行后ID：{Thread.CurrentThread.ManagedThreadId}");*/
            //return Ok();
        }
    }
}
