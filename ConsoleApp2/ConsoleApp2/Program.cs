﻿using MiniExcelLibs;
using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace ConsoleApp2
{
    internal class Program
    {
        static Dictionary<string, string> municipality = new Dictionary<string, string> { 
            { "110000", "北京市" },
            { "120000", "天津市" },
            { "310000", "上海市" },
            { "500000", "重庆市" }
        };

        static async Task Main(string[] args)
        {
            var list = await MiniExcel.QueryAsync<行政区域>(Path.GetFullPath("../../../data.xlsx"), sheetName: "行政区划代码");

            //Console.WriteLine(GetAreas(new 行政区域 { 代码 = "330000", 名称 = "浙江省" }));

            List<Areas> areas = new List<Areas>();

            foreach (var item in list)
            {
                areas.Add(GetAreas(item));
            }

            foreach (var item in municipality)
            {
                areas.Add(new Areas
                {
                    Code = (item.Key.Remove(item.Key.LastIndexOf('0')) + 1).ToString(),
                    Level = 1,
                    Name = item.Value,
                    ParentId = item.Key
                });
            }

            

            //areaall.js
            var areaall_json = JsonConvert.SerializeObject(areadatas(areas, "0"));

            using (StreamWriter sw = new StreamWriter(Path.GetFullPath("../../../areaall.json")))
            {
                sw.WriteLine(areaall_json);
            }

            //area.js
            area area = new area();
            area.province_list = pairs(areas);
            area.city_list = pairs(areas,1);
            area.country_list = pairs(areas,2);

            var area_json = JsonConvert.SerializeObject(area);
            using (StreamWriter sw = new StreamWriter(Path.GetFullPath("../../../area.json")))
            {
                sw.WriteLine(area_json);
            }

            Console.WriteLine("生成成功");
            Console.ReadLine();
        }

        /// <summary>
        /// 格式化所有的行政区域编码
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static Areas GetAreas(行政区域 item)
        {
            //直辖市            
            Areas area = new Areas();
            area.Code = item.代码;
            area.Name = item.名称;
            if (municipality.ContainsKey(item.代码))
            {
                area.ParentId = "0";
                area.Level = 0;
            }
            else if(item.代码.StartsWith("11") || item.代码.StartsWith("12")|| item.代码.StartsWith("31")|| item.代码.StartsWith("50"))
            {
                area.ParentId = item.代码.Substring(0, 2).PadRight(5, '0') + "1";
                area.Level = 2;
            }
            else if (item.代码.TrimEnd('0').Length == 1 || item.代码.TrimEnd('0').Length == 2)
            {
                area.ParentId = "0";
                area.Level = 0;
            }
            else if (item.代码.TrimEnd('0').Length == 3 || item.代码.TrimEnd('0').Length == 4)
            {
                area.ParentId = item.代码.Substring(0, 2).PadRight(6, '0');
                area.Level = 1;
            }
            else if (item.代码.TrimEnd('0').Length == 5 || item.代码.TrimEnd('0').Length == 6)
            {
                area.ParentId = item.代码.Substring(0, 4).PadRight(6, '0');
                area.Level = 2;
            }
            else
            {
                area.ParentId = "未知";
                area.Level = -1;
            }
            return area;
        }

        /// <summary>
        /// areasall.js--递归获取
        /// </summary>
        /// <param name="areas"></param>
        /// <returns></returns>
        public static List<areadata> areadatas(List<Areas> areas,string ParentId)
        {
            List<areadata> areaList = new List<areadata>();

            var list = areas.Where(m => m.ParentId == ParentId);

            foreach (Areas area in list)
            {
                areadata areadata = new areadata {
                    value = area.Code,
                    label = area.Name,
                    children = !areadatas(areas, area.Code).Any() ? null : areadatas(areas, area.Code)
                };

                areaList.Add(areadata);
            }

            return areaList;
        }

        /// <summary>
        /// 返回字典值
        /// </summary>
        /// <param name="areas"></param>
        /// <param name="Level"></param>
        /// <returns></returns>
        public static Dictionary<string,string> pairs(List<Areas> areas,int Level = 0)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            foreach (Areas area in areas.Where(m=>m.Level == Level)) 
            { 
                dic.Add(area.Code,area.Name);
            }
            return dic;
        }
    }

    /// <summary>
    /// area.js
    /// </summary>
    public class area
    {
        public Dictionary<string,string> province_list { get; set; }
        public Dictionary<string,string> city_list { get; set; }
        public Dictionary<string,string> country_list { get; set; }
    }

    /// <summary>
    /// areaall.js--Model
    /// </summary>
    public class areadata
    {
        public string value { get; set; }
        public string label { get; set; }
        public List<areadata> children { get; set; }
    }

    /// <summary>
    /// 所有省市县
    /// </summary>
    public class Areas
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string ParentId { get; set; }
        public int Level { get; set; }
    }

    /// <summary>
    /// 从Excel接收Model
    /// </summary>
    public class 行政区域
    {
        public string 代码 { get; set; }
        public string 名称 { get; set; }
    }
}