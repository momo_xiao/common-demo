﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace 用户管理系统.ServiceControl
{
    /// <summary>
    /// ListUserControl.xaml 的交互逻辑
    /// </summary>
    public partial class ListUserControl : UserControl
    {
        private List<Customer> _customers;
        private int _pageSize = 10;
        private int _pageIndex = 0;

        public ListUserControl()
        {
            InitializeComponent();
            LoadData();
        }

        private void LoadData()
        {
            // 模拟从数据库中获取数据
            _customers = new List<Customer>();
            for (int i = 1; i <= 100; i++)
            {
                _customers.Add(new Customer { Id = i, Name = "Customer " + i });
            }
            BindData();
        }

        private void BindData()
        {
            // 设置数据源
            var pageSize = _pageSize;
            var pageIndex = _pageIndex;
            var query = _customers.Skip(pageSize * pageIndex).Take(pageSize);
            MyDataGrid.ItemsSource = query;

            // 设置分页信息
            var totalCount = _customers.Count;
            var pageCount = (int)Math.Ceiling(totalCount / (double)pageSize);
            var start = pageIndex * pageSize + 1;
            var end = Math.Min(start + pageSize - 1, totalCount);
            var pageInfo = $"Page {pageIndex + 1} of {pageCount}, Total {totalCount} records, Showing {start}-{end}";
            this.DataContext = new { Customers = query, PageInfo = pageInfo };
        }

        private void FirstPage_Click(object sender, RoutedEventArgs e)
        {
            _pageIndex = 0;
            BindData();
        }

        private void PrevPage_Click(object sender, RoutedEventArgs e)
        {
            if (_pageIndex > 0)
            {
                _pageIndex--;
                BindData();
            }
        }

        private void NextPage_Click(object sender, RoutedEventArgs e)
        {
            var pageCount = (int)Math.Ceiling(_customers.Count / (double)_pageSize);
            if (_pageIndex < pageCount - 1)
            {
                _pageIndex++;
                BindData();
            }
        }

        private void LastPage_Click(object sender, RoutedEventArgs e)
        {
            var pageCount = (int)Math.Ceiling(_customers.Count / (double)_pageSize);
            _pageIndex = pageCount - 1;
            BindData();
        }
    }

    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
