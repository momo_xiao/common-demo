﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System;
using Utils.Zip;
using System.Reflection.PortableExecutable;
using System.Xml.Linq;
using PdfSharpCore.Drawing;
using PdfSharpCore.Pdf;
using PdfSharpCore.Pdf.IO;
using PdfSharpCore;

namespace DownLoadFile.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DownloadController : ControllerBase
    {
        private readonly IWebHostEnvironment environment;

        public DownloadController(IWebHostEnvironment environment)
        {
            this.environment = environment;
        }

        /// <summary>
        /// 远程文件下载
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> SaveAs(List<string> url)
        {
            //创建临时文件夹
            var FolderPath = $"{environment.WebRootPath}/Download/{Guid.NewGuid()}";
            if (!Directory.Exists(FolderPath))
            {
                Directory.CreateDirectory(FolderPath);
            }

            //下载所有文件
            foreach (var item in url)
            {
                HttpClient client = new HttpClient();
                var file = await client.GetStreamAsync(item);


                using (FileStream fileStream = new FileStream($"{FolderPath}/{Guid.NewGuid()}.pdf", FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }

            //文件压缩
            ZipHelper zipHelper = new ZipHelper();
            var zipResult = zipHelper.CreatZip(FolderPath, $"{environment.WebRootPath}/Download/文件下载.zip");

            //删除之前的临时文件夹
            if (Directory.Exists(FolderPath))
            {
                Directory.Delete(FolderPath, true);
            }

            return Ok(zipResult);
        }

        /// <summary>
        /// 文件下载
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> OutputFile()
        {
            MemoryStream memory = new MemoryStream();
            using (FileStream file = new FileStream(@"E:\Tech\RbacSystem\RbacSystem\wwwroot\UploadFile\202302\20230207\20230207155000.jpg", FileMode.Open))
            {
                await file.CopyToAsync(memory);
            }
            return File(memory.ToArray(), "image/jpeg");
        }

        [HttpGet("pdf")]
        public IActionResult pdf()
        {
            string inputFile = @"E:\Tech\常用Demo\DownLoadFile\wwwroot\Download\20220803005545.pdf";
            string outputFile = $@"E:\Tech\常用Demo\DownLoadFile\wwwroot\Download\{Guid.NewGuid().ToString("n")}.pdf";
            string watermarkText = "Confidential";

            // Load the PDF file
            PdfDocument inputDocument = PdfReader.Open(inputFile, PdfDocumentOpenMode.Modify);

            // Add watermark to each page
            foreach (PdfPage page in inputDocument.Pages)
            {
                // Create graphics object
                XGraphics gfx = XGraphics.FromPdfPage(page);

                // Set font and color for watermark text
                XFont font = new XFont("Arial", 64, XFontStyle.BoldItalic);

                // 获取页面大小
                var pageSize = page;

                // Calculate watermark position and angle
                XSize textSize = gfx.MeasureString(watermarkText, font);

                
                for (int i = 0; i < 2; i++)
                {
                    // 将水印文本写入页面
                    gfx.RotateAtTransform(30, new XPoint(textSize.Width / 2 + 490 * i, textSize.Height / 2 + 90 * i));
                    gfx.DrawString(watermarkText, font, new XSolidBrush(XColor.FromArgb(128, 0, 0, 0)),
                        new XPoint(30 + 350 * i, 60 + 90 * i));
                    gfx.RotateAtTransform(-30, new XPoint(textSize.Width / 2 + 490 * i, textSize.Height / 2 + 90 * i));
                }
            }

            // Save the modified PDF file
            inputDocument.Save(outputFile);

            // Release resources
            inputDocument.Close();
            return Ok();
        }
    }
}
