using RedLockNet.SERedis.Configuration;
using RedLockNet.SERedis;
using RedLockNet;
using System.Net;
using RedLock分布式锁;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

#region 创建分布式锁
//创建分布式锁
var redisUrl = builder.Configuration["RedisUrl"];
if (string.IsNullOrEmpty(redisUrl))
{
    throw new ArgumentException("RedisUrl 不能为空");
}
var urls = redisUrl.Split(",").ToList();
var endPoints = new List<RedLockEndPoint>();
foreach (var item in urls)
{
    var arr = item.Split(":");
    endPoints.Add(new DnsEndPoint(arr[0], Convert.ToInt32(arr[1])));
}
RedLockFactory lockFactory = RedLockFactory.Create(endPoints);

//注入锁和服务
builder.Services.AddSingleton(typeof(IDistributedLockFactory), lockFactory);
builder.Services.AddScoped(typeof(ProductService));
#endregion

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

#region 应用生命周期释放分布式锁
app.Lifetime.ApplicationStopping.Register(() =>
{
    lockFactory.Dispose();
});
#endregion