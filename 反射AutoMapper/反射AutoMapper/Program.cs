﻿using Application;
using Domain;

namespace 反射AutoMapper
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Member member = new Member {
                Age = 1,
                Birthday = DateTime.Now,
                MemberId = 10,
                MemberName = "张三"
            };

            MemberDto dto = member.Map<Member, MemberDto>();
        }
    }

    public static class Mapper
    {
        public static TTarget Map<TSrouce,TTarget>(this TSrouce srouce) 
            where TSrouce : class
            where TTarget : class, new()
        {
            TTarget target = new TTarget();

            var sourceProperties = typeof(TSrouce).GetProperties();

            var targetProperties = typeof(TTarget).GetProperties();

            foreach (var item in targetProperties)
            {
                var prop = sourceProperties.FirstOrDefault(m => m.Name == item.Name);

                if(prop != null)
                {
                    var value = prop.GetValue(srouce);
                    item.SetValue(target, value);
                }
            }

            return target;
        }
    }
}