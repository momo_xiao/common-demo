﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NPOI复杂嵌套导出
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region 这是模拟你数据库里的数据            
            //List<Device> Devices = new List<Device>();
            List<GlucoseLog> logs = new List<GlucoseLog>();
            for (int i = 0; i < 65; i++)
            {
                Guid guid = Guid.NewGuid();
                //Devices.Add(new Device { TransId = guid });
                Random rnd = new Random();

                for (int l = 0; l < rnd.Next(10, 60); l++)
                {
                    //每个设备每天添加6条记录
                    for (int d = 0; d < 6; d++)
                    {
                        logs.Add(new GlucoseLog
                        {
                            Id = Guid.NewGuid(),
                            TransId = guid,
                            CreateTime = DateTime.Now.AddDays(l)
                        });
                    }
                }
            }
            #endregion

            //获取当前应用程序目录
            var basePath = Path.GetFullPath("../../../Excel", Directory.GetCurrentDirectory());

            //创建Excel文件夹
            if (Directory.Exists(basePath))
                Directory.CreateDirectory(basePath);

            //按时间分组，并生成文件夹，并生成文件夹
            var groupTime = logs.GroupBy(m => m.CreateTime.Date).Select(m => m.Key);

            //然后遍历groupTime，查到每一天的记录
            foreach (var CurrentTime in groupTime)
            {
                //查到当天的设备Id，这里你也可以直接查仓储
                var currentDayList = logs.Where(m => m.CreateTime.Date == CurrentTime.Date).GroupBy(m => m.TransId)
                    .Select(m => m.Key);
                if (currentDayList.Count() > 0)
                {
                    //开始创建当天的文件夹
                    var FolderPath = Directory.CreateDirectory($"{basePath}/{CurrentTime.Date.ToString("yyyy-MM-dd")}");

                    //每多少个Sheet生成一个工作簿
                    int PageSize = 10;

                    //从第1个工作簿开始
                    int PageIndex = 1;

                    //总页数，也就是总的工作簿数
                    int PageCount = Convert.ToInt32(Math.Ceiling(currentDayList.Count() / (PageSize * 1.0)));

                    ////遍历循环外层数据
                    for (int i = PageIndex; i <= PageCount; i++)
                    {
                        //获取当前页要获取的设备的数据，也就是TransId
                        var list = currentDayList.Skip((i - 1) * PageSize).Take(PageSize);

                        //这是一个Workbook，也就是一个工作簿
                        HSSFWorkbook hssfworkbook = new HSSFWorkbook();

                        //list集合里的每一条记录生成一个sheet
                        var l = 0;
                        foreach (var item in list)
                        {
                            //找到内层数据，当前Sheet的数据
                            var logList = logs.Where(m => m.TransId == item && m.CreateTime.Date == CurrentTime.Date);

                            //创建一个sheet，用来存放当前transid的记录
                            ISheet sheet = hssfworkbook.CreateSheet($"Sheet{l + 1}");

                            //设置列宽
                            sheet.SetColumnWidth(0, 50 * 256);
                            sheet.SetColumnWidth(1, 50 * 256);
                            sheet.SetColumnWidth(2, 20 * 256);

                            //表头
                            IRow rowHeader = sheet.CreateRow(0);
                            rowHeader.CreateCell(0).SetCellValue("佩戴记录Id");
                            rowHeader.CreateCell(1).SetCellValue("设备Id");
                            rowHeader.CreateCell(2).SetCellValue("创建时间");

                            var r = 1;
                            //遍历获取当前Sheet需要的数据
                            foreach (var log in logList)
                            {
                                //创建Excel行
                                IRow row = sheet.CreateRow(r);

                                //根据行创建Excel列
                                row.CreateCell(0).SetCellValue(log.Id.ToString());
                                row.CreateCell(1).SetCellValue(log.TransId.ToString());
                                row.CreateCell(2).SetCellValue(log.CreateTime.ToString("yyyy-MM-dd"));
                                r++;
                            }

                            l++;
                        }

                        //这是保存的文件
                        using (FileStream file = new FileStream($"{FolderPath}/Log{i}.xlsx", FileMode.Create))
                        {
                            hssfworkbook.Write(file);
                        }
                    }
                }
            }


            Console.WriteLine("导出成功......");
            Console.ReadLine();
        }
    }

    /// <summary>
    /// 佩戴设备
    /// </summary>
    public class Device
    {
        public Guid TransId { get; set; }
        public List<GlucoseLog> Logs { get; set; }
    }

    /// <summary>
    /// 佩戴记录
    /// </summary>
    public class GlucoseLog
    {
        public Guid Id { get; set; }
        public Guid TransId { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
