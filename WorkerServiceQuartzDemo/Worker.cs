using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Quartz.Impl;
using Quartz;
using Quartz.Spi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace WorkerServiceQuartzDemo
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IJobFactory factory;

        public Worker(ILogger<Worker> logger, IJobFactory factory)
        {
            _logger = logger;
            this.factory = factory;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            //调度器  触发器  作业
            //1、创建调度器
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            var scheduler = await schedulerFactory.GetScheduler();

            //2、作业
            //var jobData = new JobDataMap();
            //jobData.Add(nameof(jobData), $"作业数据{Guid.NewGuid()}");
            //jobData.Add(nameof(CSRedisClient), client);

            IJobDetail jobDetail = JobBuilder.Create<AutoJobData>()
                                    //.SetJobData(jobData)
                                    .WithIdentity(nameof(AutoJobData), "RedisQuartz")
                                    .WithDescription("这是一个基于Quartz.Net实现的作业调度示例")
                                    .Build();

            //3、创建触发器
            ITrigger trigger = TriggerBuilder.Create()
                                .WithIdentity(
                                $"{nameof(AutoJobData)}_{nameof(ITrigger)}",
                                $"{nameof(ITrigger)}Group")
                                .WithDescription("这是一个基于Quartz.Net实现的作业调度示例----触发器")
                                .WithSimpleSchedule(builder =>
                                {
                                    builder
                                    .WithIntervalInSeconds(3)
                                    .WithRepeatCount(5);
                                })
                                //.WithCronSchedule("5,15,38,42 * * * * ?")
                                .Build();


            //配置JobFactory
            scheduler.JobFactory = factory;

            //4、通过调度器实现任务调度
            await scheduler.ScheduleJob(jobDetail, trigger);
            await scheduler.Start();
        }
    }
}
