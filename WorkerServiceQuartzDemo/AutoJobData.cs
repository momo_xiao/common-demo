﻿using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerServiceQuartzDemo
{
    public class AutoJobData : IJob
    {
        private readonly ILogger<AutoJobData> logger;

        public AutoJobData(ILogger<AutoJobData> logger)
        {
            this.logger = logger;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            logger.LogInformation(DateTime.Now.ToString());
        }
    }
}
