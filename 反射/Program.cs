﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace 反射
{
    public class Program
    {
        public static async Task Main()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Id", typeof(Guid));
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Birthday", typeof(DateTime));

            DataRow dr = dt.NewRow();
            dr["Id"] = Guid.NewGuid();
            dr["Name"] = "张三";
            dr["Birthday"] = DateTime.Now;
            dt.Rows.Add(dr);

            Console.Read();

            Orm<Admin> orm = new Orm<Admin>("Data Source=.;Initial Catalog=AuthRbacSystem;Integrated Security=True");

            await orm.QueryAll();

            Console.ReadLine();
        }
    }


    public class Orm<TEntity>
    {
        private string _ConnStr;

        public Orm(string ConnStr)
        {
            _ConnStr = ConnStr;
        }

        /// <summary>
        /// 1、通过反射动态获取entity里面的属性以及属性值
        /// 2、根据这些属性和属性值去拼接SQL
        /// 3、利用ADO.NET，去执行SQL      CLR  CommonLanguageRuntime  公共语言运行时
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<int> Add(TEntity entity)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_ConnStr))
                {
                    Type type = entity.GetType();


                    var fiels = type.GetProperties().Where(p => p.GetCustomAttributes().Count(s => s is PrimaryKeyAttribute) == 0).ToList();

                    string Sql = $"INSERT INTO {type.Name} ({string.Join(',', fiels.Select(m => m.Name))})VALUES({string.Join(',', fiels.Select(m => $"@{m.Name}"))})";

                    SqlCommand cmd = new SqlCommand(Sql, conn);

                    await conn.OpenAsync();

                    SqlParameter[] sqlParams = fiels.Select(m => m.GetValue(entity) == null ? new SqlParameter(m.Name, DBNull.Value) : new SqlParameter(m.Name, m.GetValue(entity))).ToArray();

                    cmd.Parameters.AddRange(sqlParams);

                    return await cmd.ExecuteNonQueryAsync();
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }


        public async Task<List<TEntity>> Query()
        {
            try
            {
                using SqlConnection conn = new SqlConnection(_ConnStr);

                Type type = typeof(TEntity);

                string Sql = $"SELECT * FROM {type.Name}";

                SqlDataAdapter adapter = new SqlDataAdapter(Sql, conn);

                DataSet dataSet = new DataSet();

                adapter.Fill(dataSet);

                DataTable dt = dataSet.Tables[0];

                string json = JsonConvert.SerializeObject(dt);

                List<TEntity> list = JsonConvert.DeserializeObject<List<TEntity>>(json);

                return list;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<List<TEntity>> QueryAll()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection("Data Source=.;Initial Catalog=BookStore;Integrated Security=True"))
                {
                    string Sql = $"SELECT * FROM App_Author;SELECT * FROM App_Book";

                    await conn.OpenAsync();

                    SqlCommand cmd = new SqlCommand(Sql, conn);

                    using (SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            Console.WriteLine(reader[0]);
                        }

                        await reader.NextResultAsync();

                        while (reader.Read())
                        {
                            Console.WriteLine(reader[0]);
                        }
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }

    public class Admin
    {
        [PrimaryKey]
        public int AdminId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public DateTime? LastLoginTime { get; set; }
        public string LastLoginIP { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class PrimaryKeyAttribute : Attribute
    {

    }
}
