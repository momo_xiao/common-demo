﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TreeDemo
{
    public class equip_type
    {
        public string Id { get; set; }
        public string Name { get; set; }

        [Column("parent_id")]
        public string ParentId { get; set; }
    }

    public class TreeDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ParentId { get; set; }

        public IList<TreeDto> Children { get; set; } = new List<TreeDto>();
    }
}
