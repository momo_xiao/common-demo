﻿namespace 工厂方法
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ILightFactory tubeFactory = new TubeFactory();
            ILight light1 = tubeFactory.Create();
            light1.On();
            light1.Off();

            ILightFactory bulbFactory = new BulbFactory();
            ILight light2 = bulbFactory.Create();
            light2.On();
            light2.Off();

            //开闭原则——对扩展开放，对修改封闭
            ILightFactory lightFactory = new ProductFactory();
            ILight light3 = lightFactory.Create();
            light3.On();
            light3.Off();

            Console.ReadLine();
        }
    }

    /// <summary>
    /// 抽象产品
    /// </summary>
    public interface ILight
    {
        void On();
        void Off();
    }

    public class BulbLight : ILight
    {
        public void Off()
        {
            Console.WriteLine("off");
        }

        public void On()
        {
            Console.WriteLine("on");
        }
    }

    /// <summary>
    /// 具体产品
    /// </summary>
    public class TubeLight : ILight
    {
        public void Off()
        {
            Console.WriteLine("off");
        }

        public void On()
        {
            Console.WriteLine("on");
        }
    }

    public class Product : ILight
    {
        public void Off()
        {

        }

        public void On()
        {

        }
    }

    /// <summary>
    /// 抽象工厂类
    /// </summary>
    public interface ILightFactory
    {
        ILight Create();
    }

    public class ProductFactory : ILightFactory
    {
        public ILight Create()
        {
            return new Product();
        }
    }

    /// <summary>
    /// 具体工厂——灯泡工厂类
    /// </summary>
    public class BulbFactory : ILightFactory
    {
        public ILight Create()
        {
            return new BulbLight();
        }
    }

    /// <summary>
    /// 灯管工厂类
    /// </summary>
    public class TubeFactory : ILightFactory
    {
        public ILight Create()
        {
            return new TubeLight();
        }
    }
}
