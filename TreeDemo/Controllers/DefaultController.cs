﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace TreeDemo.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DefaultController : ControllerBase
    {
        private readonly DefaultDbContext db;

        public DefaultController(DefaultDbContext db)
        {
            this.db = db;
        }

        /// <summary>
        /// 非递归实现，相当于空间换时间，速度优于递归
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Tree()
        {
            var list = await db.equip_type.ToListAsync();

            var tree = list.Select(m => new TreeDto
            {
                Id = m.Id,
                Name = m.Name,
                ParentId = m.ParentId
            });

            Dictionary<string, TreeDto> dic = new Dictionary<string, TreeDto>();
            foreach (var item in tree)
            {
                dic.Add(item.Id, item);
            }

            foreach (var item in dic.Values)
            {
                if (!string.IsNullOrEmpty(item.ParentId))
                {
                    if (dic.ContainsKey(item.ParentId))
                    {
                        dic[item.ParentId].Children.Add(item);
                    }
                }
            }

            return new JsonResult(dic.Values.Where(m => string.IsNullOrEmpty(m.ParentId)));
        }

        /// <summary>
        /// 递归实现
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Nodes()
        {
            var list = await db.equip_type.ToListAsync();

            var tree = list.Select(m => new TreeDto
            {
                Id = m.Id,
                Name = m.Name,
                ParentId = m.ParentId
            }).ToList();

            var rootNode = tree.Where(m => string.IsNullOrEmpty(m.ParentId)).ToList();
            foreach (var item in rootNode)
            {
                Recursion(item, tree);
            }

            return new JsonResult(rootNode);
        }

        /// <summary>
        /// 递归方法
        /// </summary>
        /// <param name="dto">当前要查询子节点的节点</param>
        /// <param name="list">所有的节点信息</param>
        private void Recursion(TreeDto dto, List<TreeDto> list)
        {
            var subNode = list.Where(m => m.ParentId == dto.Id);
            foreach (var item in subNode)
            {
                dto.Children.Add(item);
                Recursion(item, list);
            }
        }
    }
}
