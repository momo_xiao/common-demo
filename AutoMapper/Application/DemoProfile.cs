﻿using AutoMapper;
using Domain;

namespace Application
{
    public class DemoProfile : Profile
    {
        public DemoProfile()
        {
            CreateMap<Member,MemberDto>().ReverseMap();
        }
    }
}