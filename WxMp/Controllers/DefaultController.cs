﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using System;
using WxMp.WxModels;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Xml.Linq;
using Microsoft.AspNetCore.Hosting;
using System.Net;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Collections.Generic;

namespace WxMp.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DefaultController : ControllerBase
    {
        private readonly ILogger<DefaultController> logger;
        private readonly IDistributedCache cache;
        private readonly IConfiguration configuration;
        private readonly IWebHostEnvironment webHost;
        private readonly IWebHostEnvironment env;

        public DefaultController(
            ILogger<DefaultController> logger,
            IDistributedCache cache,
            IConfiguration configuration,
            IWebHostEnvironment webHost,
            IWebHostEnvironment env
            )
        {
            this.logger = logger;
            this.cache = cache;
            this.configuration = configuration;
            this.webHost = webHost;
            this.env = env;
        }

        //[HttpPost]
        public async Task<IActionResult> Index(string signature, string timestamp, string nonce, string echostr)
        {
            /*
            using (StreamReader reader = new StreamReader(Request.Body))
            {
                string xml = await reader.ReadToEndAsync();
                //存入StringReader
                StringReader sr = new StringReader(xml);
                var el = XElement.Load(sr);

                var ToUserName = ((XElement)el.Nodes().First(m => (m as XElement).Name.LocalName == "ToUserName")).Value;
                var FromUserName = ((XElement)el.Nodes().First(m => (m as XElement).Name.LocalName == "FromUserName")).Value;
                var MsgType = ((XElement)el.Nodes().First(m => (m as XElement).Name.LocalName == "MsgType")).Value;

                if(MsgType == "image")
                {
                    var PicUrl = ((XElement)el.Nodes().First(m => (m as XElement).Name.LocalName == "PicUrl")).Value;
                    var MediaId = ((XElement)el.Nodes().First(m => (m as XElement).Name.LocalName == "MediaId")).Value;

                    using (HttpClient client = new HttpClient())
                    {
                        //获取token
                        var access_token = await GetToken();

                        //流
                        var image = await client.GetStreamAsync(string.Format(WxApis.getMedia, access_token, MediaId));

                        using (FileStream stream = new FileStream($"{webHost.ContentRootPath}/temp.jpg", FileMode.Create))
                        {
                            await image.CopyToAsync(stream);
                        }
                    }

                    logger.LogError(PicUrl);
                    logger.LogError(MediaId);

                    logger.LogInformation(xml);
                }

                if(MsgType == "text")
                {
                    var Content = ((XElement)el.Nodes().First(m => (m as XElement).Name.LocalName == "Content")).Value;

                    string _xml = @"<xml>
                                  <ToUserName><![CDATA[toUser]]></ToUserName>
                                  <FromUserName><![CDATA[fromUser]]></FromUserName>
                                  <CreateTime>12345678</CreateTime>
                                  <MsgType><![CDATA[text]]></MsgType>
                                  <Content><![CDATA[你好]]></Content>
                                </xml>";

                    _xml = _xml.Replace("toUser", FromUserName)
                                .Replace("fromUser", ToUserName)
                                .Replace("你好", $"你好,{Content}");

                    return Ok(_xml);
                }

                logger.LogInformation(ToUserName);
                logger.LogInformation(FromUserName);
            }
            */
            /*var body = Request.Body;

            byte[] bytes = new byte[body.Length];

            await body.ReadAsync(bytes, 0, bytes.Length);

            logger.LogInformation(Encoding.UTF8.GetString(bytes));*/

            //return Ok(echostr);

            #region 接入
            var token = "ofMiNt43RVJjrqOqo58AJhLWzcb0";

            //参数数组
            string[] param = new string[] { token, timestamp, nonce };

            //字典排序的字符串
            var constr = string.Concat(param.OrderBy(m => m));

            //加密之后
            var encr = PasswordEncryption(constr);

            if (signature == encr)
            {
                return Ok(echostr);
            }
            #endregion

            return Ok(string.Empty);
        }

        [NonAction]
        private async Task<string> GetToken()
        {
            string token = await cache.GetStringAsync(nameof(WxApis.Token));

            if (string.IsNullOrEmpty(token))
            {
                HttpClient client = new HttpClient();

                //URL
                var result = await client.GetStringAsync(string.Format(WxApis.Token, configuration["WxConfig:appId"], configuration["WxConfig:appsecret"]));

                //反序列化
                WxToken ApiResult = JsonConvert.DeserializeObject<WxToken>(result);

                //存入缓存
                await cache.SetStringAsync(nameof(WxApis.Token), ApiResult.access_token, new DistributedCacheEntryOptions { AbsoluteExpiration = DateTime.Now.AddHours(2) });
                
                token = ApiResult.access_token;
            }

            return token;
        }

        [HttpGet]
        public async Task<IActionResult> GetUser()
        {
            var token = await cache.GetStringAsync(nameof(WxApis.Token));

            using (HttpClient client = new HttpClient())
            {
                var result = await client.GetStringAsync(string.Format(WxApis.getUsers,token));
                WxUser user = JsonConvert.DeserializeObject<WxUser>(result);
                return new JsonResult(user.data);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetUserInfo(string openid)
        {
            var token = await cache.GetStringAsync(nameof(WxApis.Token));

            using (HttpClient client = new HttpClient())
            {
                var result = await client.GetStringAsync(string.Format(WxApis.getUserInfo, token, openid));
                WxUserInfo user = JsonConvert.DeserializeObject<WxUserInfo>(result);
                return Ok(user);
            }
        }

        private static string PasswordEncryption(string pwd)
        {
            //创建SHA1加密算法对象
            SHA1 sha1 = SHA1.Create();
            //将原始密码转换为字节数组
            byte[] originalPwd = Encoding.UTF8.GetBytes(pwd);
            //执行加密
            byte[] encryPwd = sha1.ComputeHash(originalPwd);
            //将加密后的字节数组转换为大写字符串
            return string.Join("", encryPwd.Select(b => string.Format("{0:x2}",
          b)).ToArray());
        }

        [HttpGet]
        public IActionResult XmlParse()
        {
            string xml = @"<xml>
                          <ToUserName>feeling</ToUserName>
                          <FromUserName>xiaojie</FromUserName>
                          <CreateTime>1348831860</CreateTime>
                          <MsgType>text</MsgType>
                          <Content>this is a test</Content>
                          <MsgId>1234567890123456</MsgId>
                          <MsgDataId>xxxx</MsgDataId>
                          <Idx>xxxx</Idx>
                        </xml>";

            StringReader sr = new StringReader(xml);

            var el = XElement.Load(sr);

            /*var ToUserName = (el.Nodes().First() as XElement);
            var ToUserName1 = (el.Nodes().First() as XElement).Name;*/

            var ToUserName = ((XElement)el.Nodes().First(m => (m as XElement).Name.LocalName == "CreateTime")).Value;

            return Ok();
        }

        /// <summary>
        /// 上传微信素材的两种方式
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> UploadLoadMedia()
        {
            string access_token = await GetToken();

            #region 方式一：通过webClient
            WebClient webClient = new WebClient();
            var media = webClient.UploadFile(string.Format(WxApis.UploadMedia, access_token, "image"), $"{env.WebRootPath}/images/02.jpg");
            var str = Encoding.Default.GetString(media);
            #endregion

            #region 方式二：通过HttpClient
            HttpClient httpClient = new HttpClient();
            using FileStream fileStream = new FileStream($"{env.WebRootPath}/images/02.jpg", FileMode.Open, FileAccess.Read);
            var boundary = DateTime.Now.Ticks.ToString("X");
            using (var content = new MultipartFormDataContent(boundary))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");
                content.Add(new StreamContent(fileStream), "buffer", "\"" + $"{env.WebRootPath}/images/02.jpg" + "\"");
                content.Headers.TryAddWithoutValidation("Content-Disposition", $"form-data; name=\"media\";filename=\"02.jpg\"");
                try
                {
                    var json = await httpClient.PostAsync(string.Format(WxApis.UploadMedia, access_token, "image"), content).Result.Content.ReadAsStringAsync();
                }
                catch (Exception ex)
                {
                    //ex.Message;
                }
            }
            #endregion
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetWxMenu()
        {
            HttpClient httpClient = new HttpClient();
            string token = await GetToken();
            string JsonResult = await httpClient.GetStringAsync(string.Format(WxApis.GetMenu, token));
            return Ok(JsonResult);
        }

        [HttpGet]
        public async Task<IActionResult> CreateMenu()
        {
            List<button> button = new List<button> {
                new button{
                 name = "培训",
                  sub_button = new List<sub_button>{
                    new sub_button{
                        type = "view",
                        name = "培训通知",
                        url = "http://106.13.147.171:8006/mp/homepage?__biz=MzU2OTIzMzgxNQ==&hid=1&sn=b02ecc8ee0a848bf5a44cd97875157e3#wechat_redirect"
                    },
                    new sub_button{
                        type = "view",
                        name = "我要交费",
                        url = "http://106.13.147.171:8006/WxMp/SelectMetting/2"
                    },
                    new sub_button{
                        type = "view",
                        name = "我要报名",
                        url = "http://106.13.147.171:8006/WxMp/Sign?id=11"
                    }
                  }
                },
                new button{
                 name = "个人中心",
                  sub_button = new List<sub_button>{
                    new sub_button{
                        type = "view",
                        name = "用户注册",
                        url = "http://106.13.147.171:8006/WxMp/Register"
                    },
                    new sub_button{
                        type = "view",
                        name = "我要绑定",
                        url = "http://106.13.147.171:8006/WxMp/BindWX"
                    },
                    new sub_button{
                        type = "click",
                        name = "我的条码",
                        key = "KEY002"
                    },
                    new sub_button{
                        type = "view",
                        name = "我的发票",
                        url = "http://106.13.147.171:8006/WxMp/ChooseTicket"
                    },
                    new sub_button{
                        type = "view",
                        name = "开票信息",
                        url = "http://106.13.147.171:8006/WxMp/Ticket"
                    }
                  }
                },
                new button{
                 name = "平台入口",
                  sub_button = new List<sub_button>{
                    new sub_button{
                        type = "view",
                        name = "医生网",
                        url = "http://www.oneplusx.org/m/"
                    },
                    new sub_button{
                        type = "view",
                        name = "医学平台",
                        url = "http://www.1plusx.org"
                    },
                    new sub_button{
                        type = "scancode_waitmsg",
                        name = "扫码带提示",
                        key = "V1002_GOOD"
                    },
                    new sub_button{
                        type = "pic_sysphoto",
                        name = "系统拍照发图",
                        key = "V1003_GOOD"
                    },
                    new sub_button{
                        type = "location_select",
                        name = "发送位置",
                        key = "V1004_GOOD"
                    }
                  }
                },
            };

            HttpClient httpClient = new HttpClient();
            string token = await GetToken();
            StringContent content = new StringContent(JsonConvert.SerializeObject(new { button = button }), Encoding.UTF8, "application/json");
            var http = await httpClient.PostAsync(string.Format(WxApis.CreateMenu, token), content);
            var json = await http.Content.ReadAsStringAsync();

            return Ok(json);
        }
    }
}
