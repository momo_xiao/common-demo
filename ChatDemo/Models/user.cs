﻿namespace ChatDemo.Models
{
    public class user
    {
        public string uid { get; set; }
        public string name { get; set; }
    }

    public class message
    {
        public string to { get; set; }
        public string msg { get; set; }
    }
}
