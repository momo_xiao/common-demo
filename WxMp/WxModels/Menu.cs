﻿using System.Collections.Generic;

namespace WxMp.WxModels
{
    public class sub_button
    {
        public string type { get; set; }
        public string name { get; set; }
        public string media_id { get; set; }
        public string article_id { get; set; }
        public string url { get; set; }
        public string key { get; set; }
        public string appid { get; set; }
        public string pagepath { get; set; }
    }

    public class button
    {
        public string name { get; set; }
        public List<sub_button> sub_button { get; set; }
    }

    public class buttons
    {
        public List<button> button { get; set; }
    }
}
