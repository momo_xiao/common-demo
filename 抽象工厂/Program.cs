﻿namespace 抽象工厂
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ILightFactory lightFactory = new TubeFactory();
            ILight light = lightFactory.CreateLight();
            IPowerSwitch powerSwitch = lightFactory.CreateSwitch();
            light.On();
            light.Off();
            powerSwitch.Switch();
            Console.ReadLine();
        }

        public interface IPowerSwitch
        {
            void Switch();
        }

        public class BulbSwitch : IPowerSwitch
        {
            public void Switch()
            {
                Console.WriteLine("灯泡开关");
            }
        }

        public class TubeSwitch : IPowerSwitch
        {
            public void Switch()
            {
                Console.WriteLine("日光灯开关");
            }
        }

        public interface ILight
        {
            void On();
            void Off();
        }

        public class BulbLight : ILight
        {
            public void Off()
            {
                Console.WriteLine("off");
            }

            public void On()
            {
                Console.WriteLine("on");
            }
        }

        public class TubeLight : ILight
        {
            public void Off()
            {
                Console.WriteLine("off");
            }

            public void On()
            {
                Console.WriteLine("on");
            }
        }

        public interface ILightFactory
        {
            ILight CreateLight();
            IPowerSwitch CreateSwitch();
        }

        public class BulbFactory : ILightFactory
        {
            public ILight CreateLight()
            {
                return new BulbLight();
            }

            public IPowerSwitch CreateSwitch()
            {
                return new BulbSwitch();
            }
        }

        public class TubeFactory : ILightFactory
        {
            public ILight CreateLight()
            {
                return new TubeLight();
            }

            public IPowerSwitch CreateSwitch()
            {
                return new TubeSwitch();
            }
        }
    }
}
