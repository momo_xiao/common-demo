﻿using System;
using System.Threading;

namespace 多线程示例
{
    class Program
    {
        static void Main(string[] args)
        {
            Test test = new Test();

            //子线程
            Thread thread = new Thread(test.Log);
            thread.Start();
            thread.Name = "子线程";

            //主线程
            test.Log();

            Console.ReadLine();
        }
    }

    class Test
    {
        object o = new object();
        public void Log()
        {
            lock (o)
            {
                Console.WriteLine("Done");
            }
        }
    }
}
