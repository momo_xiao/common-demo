﻿using Quartz;
using Quartz.Spi;
using System;

namespace RbacSystem
{
    public class DefaultScheduleServiceFactory : IJobFactory
    {
        private readonly IServiceProvider provider;

        public DefaultScheduleServiceFactory(IServiceProvider provider)
        {
            this.provider = provider;
        }

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            Type jobtype = bundle.JobDetail.JobType;

            return provider.GetService(jobtype) as IJob;
        }

        public void ReturnJob(IJob job)
        {
            var disposable = job as IDisposable;

            disposable?.Dispose();
        }
    }
}
