﻿using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace TreeDemo
{
    public class DefaultDbContext : DbContext
    {
        public DefaultDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        public DbSet<equip_type> equip_type { get; set; }
    }
}
