﻿namespace RedLock分布式锁
{
    public class ProductService
    {
        // 有10个商品库存，如果同时启动多个API服务进行测试，这里改成存数据库或其他方式
        private static int stockCount = 10;
        public async Task<(bool, int)> BuyAsync()
        {
            if (stockCount > 0)
            {
                // 模拟执行的逻辑代码花费的时间
                await Task.Delay(new Random().Next(100, 500));
                stockCount--;
                return (true, stockCount);
            }
            return (false, stockCount);
        }
    }
}
