﻿namespace Domain
{
    public class Member
    {
        public int MemberId { get; set; }
        public string MemberName { get; set; }
        public int Age { get; set; }
        public DateTime Birthday { get; set; }
    }
}