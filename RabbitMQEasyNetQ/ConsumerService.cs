﻿using EasyNetQ;
using Newtonsoft.Json;
using RabbitMQEasyNetQ.Model;

namespace RabbitMQEasyNetQ
{
    public class ConsumerService : BackgroundService
    {
        private readonly IBus bus;
        private readonly IServiceScopeFactory factory;

        public ConsumerService(IBus bus, IServiceScopeFactory factory)
        {
            this.bus = bus;
            this.factory = factory;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            ILogger logger = factory.CreateScope().ServiceProvider.GetRequiredService<ILogger<ConsumerService>>();

            await bus.PubSub.SubscribeAsync<TextMessage>("x.rabbitmq.xxx", m => {
                Console.WriteLine(m.Text);
                logger.LogInformation($"x.rabbitmq.xxx:{JsonConvert.SerializeObject(m)}");
            });
        }
    }
}
