﻿using Microsoft.Extensions.Configuration;
using System;

namespace WxMp
{
    public class WxApis
    {
        /// <summary>
        /// token
        /// </summary>
        public const string Token = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={0}&secret={1}";

        /// <summary>
        /// 获取所有用户
        /// </summary>
        public const string getUsers = "https://api.weixin.qq.com/cgi-bin/user/get?access_token={0}&next_openid=";

        /// <summary>
        /// 获取菜单
        /// </summary>
        public const string GetMenu = "https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info?access_token={0}";

        /// <summary>
        /// 创建菜单
        /// </summary>
        public const string CreateMenu = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token={0}";

        /// <summary>
        /// 获取用户基本信息
        /// </summary>
        public const string getUserInfo = "https://api.weixin.qq.com/cgi-bin/user/info?access_token={0}&openid={1}&lang=zh_CN";

        /// <summary>
        /// 获取临时素材
        /// </summary>
        public const string getMedia = "https://api.weixin.qq.com/cgi-bin/media/get?access_token={0}&media_id={1}";

        /// <summary>
        /// 新增素材
        /// </summary>
        public const string UploadMedia = "https://api.weixin.qq.com/cgi-bin/media/upload?access_token={0}&type={1}";
    }
}
