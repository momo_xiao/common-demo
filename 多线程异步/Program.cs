﻿using System;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;

namespace 多线程异步
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            //异步方法同步执行
            //异步方法异步执行
            Console.WriteLine("主线程开始");
            Console.WriteLine("主线程ID:" + Thread.GetCurrentProcessorId());

            Stopwatch stopwatch = Stopwatch.StartNew();
            /*await program.DoThing();*/

            var task1 = program.BuyRice();
            
            var task2 = program.HeatWater();

            Task.WhenAll(task1, task2).ContinueWith(t => {
                Console.WriteLine("任务执行结束");
            });

            //await program.HeatWater();

            /*await program.BuyRice();
            await program.HeatWater();*/

            stopwatch.Stop();

            Console.WriteLine("主线程结束");

            Console.WriteLine("总耗时：" + stopwatch.ElapsedMilliseconds);

            Console.ReadLine();
        }

        /// <summary>
        /// 烧水、买饭
        /// </summary>
        /// <returns></returns>
        public async Task DoThing()
        {
            Program program1 = new Program();
            var task1 = program1.HeatWater();  //等待  3
            await task1;
            var task2 = program1.BuyRice();    //等待  5            
            await task2;
        }


        /// <summary>
        /// 烧水
        /// </summary>
        public async Task HeatWater()
        {
            Console.WriteLine("烧水线程ID:" + Thread.GetCurrentProcessorId());

            Task a = Task.Run(() => {
                Console.WriteLine("开始烧水1");
                Console.WriteLine("开始烧水1ID:" + Thread.GetCurrentProcessorId());
                Thread.Sleep(3000);
            });
            
            Task b = Task.Run(() => {
                Console.WriteLine("开始烧水2");
                Console.WriteLine("开始烧水2ID:" + Thread.GetCurrentProcessorId());
                Thread.Sleep(5000);
            });

            await a;
            await b;
        }

        /// <summary>
        /// 买饭
        /// </summary>
        public async Task BuyRice()
        {
            Console.WriteLine("买饭线程ID:" + Thread.GetCurrentProcessorId());

            Task a = Task.Run(() => {
                Console.WriteLine("做饭线程ID:" + Thread.GetCurrentProcessorId());
                Console.WriteLine("做饭");               
                Thread.Sleep(6000);
            });
            await a;
        }
    }
}
