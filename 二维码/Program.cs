﻿using System.Drawing.Imaging;
using System.Web;

namespace 二维码
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
        }

        public static byte[] GetLogoQRCode(string url, string logoPath, int pixel = 5)
        {
            url = HttpUtility.UrlDecode(url);
            logoPath = Path.GetFullPath("../../abc.jpg");

            var bitmap = QrcodeHelper.GetLogoQRCode(url, logoPath, pixel);
            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, ImageFormat.Jpeg);

            return ms.ToArray();
        }
    }
}
