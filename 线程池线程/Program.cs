﻿using System;
using System.Threading;

namespace 线程池线程
{
    internal class Program
    {
        static void Main()
        {
            // 线程池大小为 5
            ThreadPool.SetMaxThreads(5, 5);

            for (int i = 0; i < 10; i++)
            {
                // 向线程池中添加任务
                //ThreadPool.QueueUserWorkItem(WorkMethod, i);
                WorkMethod(i);
            }

            Console.ReadKey();
        }

        static void WorkMethod(object num)
        {
            Console.WriteLine("WorkMethod {0} is running on thread {1}", num, Thread.CurrentThread.ManagedThreadId);

            // 模拟工作任务
            Thread.Sleep(5000);

            Console.WriteLine("WorkMethod {0} is done on thread {1}", num, Thread.CurrentThread.ManagedThreadId);
        }
    }
}
