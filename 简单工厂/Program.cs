﻿namespace 简单工厂
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            ILight light1 = LightFactory.Create(LightType.Bulb);

            light1.On();
            light1.Off();

            ILight light2 = LightFactory.Create(LightType.Tube);

            light2.On();
            light2.Off();

            Console.ReadLine();
        }
    }

    /// <summary>
    /// 抽象产品---灯
    /// </summary>
    public interface ILight
    {
        /// <summary>
        /// 开
        /// </summary>
        void On();
        /// <summary>
        /// 关
        /// </summary>
        void Off();
    }

    /// <summary>
    /// 灯泡
    /// </summary>
    public class BulbLight : ILight
    {
        public void Off()
        {
            Console.WriteLine("Blub--off");
        }

        public void On()
        {
            Console.WriteLine("Blub--on");
        }
    }

    /// <summary>
    /// 灯管
    /// </summary>
    public class TubeLight : ILight
    {
        public void Off()
        {
            Console.WriteLine("Tube--off");
        }

        public void On()
        {
            Console.WriteLine("Tube--on");
        }
    }

    /// <summary>
    /// 灯管
    /// </summary>
    public class TubeLight1 : ILight
    {
        public void Off()
        {
            Console.WriteLine("Tube--off");
        }

        public void On()
        {
            Console.WriteLine("Tube--on");
        }
    }

    /// <summary>
    /// 枚举--标识不同的灯类型
    /// </summary>
    public enum LightType
    {
        Bulb,
        Tube,
        Tube1
    }

    /// <summary>
    /// 工厂类
    /// </summary>
    public static class LightFactory
    {
        /// <summary>
        /// 创建产品——灯——多态
        /// </summary>
        /// <param name="LightType"></param>
        /// <returns></returns>
        public static ILight Create(LightType LightType)
        {
            if (LightType == LightType.Tube)
            {
                return new TubeLight();
            }
            else if (LightType == LightType.Tube1)
            {
                return new TubeLight();
            }
            else
            {
                return new BulbLight();
            }
        }
    }
}
