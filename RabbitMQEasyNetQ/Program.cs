using EasyNetQ.AutoSubscribe;
using RabbitMQEasyNetQ.Model;
using RabbitMQEasyNetQ;
using EasyNetQ;
using RabbitMQ.Client;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddHostedService<SubBackgroundService>();
builder.Services.AddHostedService<ConsumerService>();
builder.Services.AddSingleton<IConsume<MessageDto>, OrderConsumer>();

builder.Services.AddSingleton(typeof(IBus), provider => 
{
    return RabbitHutch.CreateBus("host=localhost");
});

builder.Services.AddSingleton(provider => {
    ConnectionFactory factory = new ConnectionFactory() { HostName = "localhost", Port = 5672 };
    IConnection connection = factory.CreateConnection();
    IModel channel = connection.CreateModel();
    return channel;
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseSubscribe("OrderService", Assembly.GetExecutingAssembly());


app.UseAuthorization();

app.MapControllers();

app.Run();
