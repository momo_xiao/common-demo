﻿using System.Net.Mail;
using System.Net;

try
{
    // 设置发件人的邮箱地址和密码
    string senderEmail = "your_email@example.com";
    string senderPassword = "your_password";

    // 创建一个SmtpClient对象，用于发送邮件
    SmtpClient smtpClient = new SmtpClient("smtp.example.com", 587);
    smtpClient.EnableSsl = true; // 启用SSL加密
    smtpClient.Credentials = new NetworkCredential(senderEmail, senderPassword);

    // 创建一个MailMessage对象，设置邮件的发送者、接收者、主题和正文内容
    MailMessage mailMessage = new MailMessage();
    mailMessage.From = new MailAddress(senderEmail);
    mailMessage.To.Add("recipient@example.com");
    mailMessage.Subject = "Hello from .NET 6";
    mailMessage.Body = "This is a test email sent from a .NET 6 application.";

    // 使用SendMailAsync异步发送邮件
    await smtpClient.SendMailAsync(mailMessage);

    Console.WriteLine("Email sent successfully.");
}
catch (Exception ex)
{
    Console.WriteLine("Error sending email: " + ex.Message);
}