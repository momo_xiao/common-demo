﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mail;
using System.Net;
using EmailSender.Model;
using System.Text;

namespace EmailSender.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IWebHostEnvironment env;

        public HomeController(IWebHostEnvironment env)
        {
            this.env = env;
        }

        [HttpGet]
        public async Task<IActionResult> Send()
        {
            try
            {
                // 设置发件人的邮箱地址和密码
                string senderEmail = "13693288263@126.com";
                string senderPassword = "JPMETYKNIWAYULMT";

                // 创建一个SmtpClient对象，用于发送邮件
                SmtpClient smtpClient = new SmtpClient("smtp.126.com", 25);
                smtpClient.EnableSsl = true; // 启用SSL加密
                smtpClient.Credentials = new NetworkCredential(senderEmail, senderPassword);

                // 创建一个MailMessage对象，设置邮件的发送者、接收者、主题和正文内容
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(senderEmail);
                //接收人邮件地址
                mailMessage.To.Add("515856410@qq.com");
                //邮件标题
                mailMessage.Subject = "Hello from .NET 6";
                //允许HTML
                mailMessage.IsBodyHtml = true;

                

                //邮件列表数据
                List<MailEntity> list = new List<MailEntity> {
                    new MailEntity
                    {
                        field1 = "强势成长",
                        field2 = "长春高新强势成长，基本面、技术面等各维度表现优秀",
                        field3 = "综合评分98分，跑赢全市场89%的股票，EPS(TTM)环比提升24.05%，目前市值为78.90亿" 
                    },
                    new MailEntity
                    {
                        field1 = "强势成长1",
                        field2 = "长春高新强势成长，基本面、技术面等各维度表现优秀1",
                        field3 = "综合评分98分，跑赢全市场89%的股票，EPS(TTM)环比提升24.05%，目前市值为78.90亿1"
                    }
                };

                //邮件内容
                using (StreamReader reader = new StreamReader($"{env.WebRootPath}/email.html"))
                {
                    string htmlTemplate = reader.ReadToEnd();
                    StringBuilder sb = new StringBuilder();
                    list.ForEach(x => {
                        sb.Append("<div style=\"border-bottom: 1px solid #D9DEE8;margin-bottom: 18px;\">");
                        sb.Append($"<div style=\"font-size: 14px;color: #022360;padding-top: 3px;padding-bottom: 13px;\">{x.field1}</div>");
                        sb.Append($"<div style=\"font-size: 20px;font-weight: 600;color: #16161A;padding-top: 4px;padding-bottom: 14px;\">{x.field2}</div>");
                        sb.Append($"<div style=\"font-size: 13px;color: #5F616D;padding-top: 5px;padding-bottom: 22px;\">{x.field3}</div>\r\n");
                        sb.Append("<div style=\"text-align: right;padding-bottom: 15px;\">");
                        sb.Append("<a style=\"font-size:14px;font-weight: 500;color:#1A75FF;text-decoration: none;\" href=\"\">查看全部&nbsp;></a>");
                        sb.Append("</div>");
                        sb.Append("</div>");
                    });

                    mailMessage.Body = htmlTemplate.Replace("{{list}}", sb.ToString());
                }

                // 发送邮件
                await smtpClient.SendMailAsync(mailMessage);

                return Ok("邮件发送成功");
            }
            catch (Exception ex)
            {
                return Ok("邮件发送失败");
            }
        }
    }
}
