﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Task示例
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            Person person1 = new Person();
            person1.Name = "John";


            person1.abc("abcc",10,"aaa");
            person1.abc(Page: 10, Name: "张三");




            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            Program program = new Program();

            var task1 = program.WriteDateTime();

            var task2 = program.OffsetDateTime(4);

            await task1;
            await task2;

            Console.WriteLine("++++++++++++");

            stopwatch.Stop();

            Console.WriteLine("总时间" + stopwatch.ElapsedMilliseconds / 1000);

            Console.ReadLine();
        }

        public async Task<(DateTime,DateTime)> All()
        {
            Program program = new Program();

            Task task1 = program.WriteDateTime();

            Task<DateTime> task2 = program.OffsetDateTime(20);

            Task<DateTime> task3 = program.OffsetDateTime();

            await task1;

            return (await task2, await task3);
        }

        public async Task WriteDateTime()
        {
            await Task.Run(() => {
                Thread.Sleep(TimeSpan.FromSeconds(2));
                Console.WriteLine(DateTime.Now);
            });
        }

        public async Task<DateTime> OffsetDateTime(int offset)
        {
            return await Task.Run<DateTime>(() => {
                Thread.Sleep(TimeSpan.FromSeconds(offset));
                Console.WriteLine(offset);
                return DateTime.Now.AddDays(offset);
            });
        }

        public async Task<DateTime> OffsetDateTime()
        {
            return await Task.Run<DateTime>(() => {
                Thread.Sleep(TimeSpan.FromSeconds(4));
                return DateTime.Now.AddDays(4);
            });
        }
    }

    partial class Person
    {
        public string Name;

        public void abc(string Name, int Page = 10, params string[] abc)
        {

        }
    }

    partial class Person
    {

    }
}
