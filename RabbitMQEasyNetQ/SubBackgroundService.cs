﻿using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System.Text;

namespace RabbitMQEasyNetQ
{
    public class SubBackgroundService : BackgroundService
    {
        private readonly IModel channel;

        public SubBackgroundService(IModel channel)
        {
            this.channel = channel;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            string queueName = "my-queue.test";

            channel.QueueDeclare(queue: queueName, durable: false, exclusive: false, autoDelete: false);

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += (sender, e) => {

                var body = e.Body;

                var message = Encoding.UTF8.GetString(body.ToArray());

                Console.WriteLine($"接收到消息：{message}");
            };

            //消费消息
            channel.BasicConsume(queue: queueName, autoAck: true, consumer: consumer);

            return Task.CompletedTask;
        }
    }
}
