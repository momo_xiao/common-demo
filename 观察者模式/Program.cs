﻿using System;
using System.Collections.Generic;

namespace 观察者模式
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //创建主题
            Subject subject = new Subject();

            //创建观察者
            Observer observer1 = new Observer("Observer 1");
            Observer observer2 = new Observer("Observer 2");
            Observer observer3 = new Observer("Observer 3");

            //将观察者添加到主题的观察者列表中
            subject.Attach(observer1);
            subject.Attach(observer2);
            subject.Attach(observer3);

            //模拟主题状态改变
            subject.State = "new state";

            //将一个观察者从主题的观察者列表中移除
            subject.Detach(observer1);

            //模拟主题状态再次改变
            subject.State = "another state";

            Console.ReadKey();
        }
    }

    //主题接口，定义了添加、移除和通知观察者的方法
    public interface ISubject
    {
        void Attach(IObserver observer);  //添加观察者
        void Detach(IObserver observer);  //移除观察者
        void Notify();  //通知观察者
    }

    //观察者接口，定义了接收该主题通知的方法
    public interface IObserver
    {
        void Update(ISubject subject);  //更新通知
    }

    //主题实现，记录状态并通知观察者
    public class Subject : ISubject
    {
        private string _state;
        private List<IObserver> _observers = new List<IObserver>();

        public void Attach(IObserver observer)
        {
            _observers.Add(observer);
        }

        public void Detach(IObserver observer)
        {
            _observers.Remove(observer);
        }

        public void Notify()
        {
            foreach (IObserver observer in _observers)
            {
                observer.Update(this);
            }
        }

        public string State
        {
            get { return _state; }
            set
            {
                _state = value;
                Notify();  //状态改变时通知观察者
            }
        }
    }

    //观察者实现
    public class Observer : IObserver
    {
        private string _name;

        public Observer(string name)
        {
            _name = name;
        }

        public void Update(ISubject subject)
        {
            Console.WriteLine("Observer {0} has received the subject's notification. New state: {1}", _name, ((Subject)subject).State);
        }
    }

}
