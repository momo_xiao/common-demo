using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Quartz.Spi;
using RbacSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace WorkerServiceQuartzDemo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var host = Host.CreateDefaultBuilder(args);

            IHostBuilder builder = host.ConfigureServices((hostContext, services) =>
            {
                services.AddSingleton<IJobFactory, DefaultScheduleServiceFactory>();
                services.AddHostedService<Worker>();
            })
            .UseServiceProviderFactory(new AutofacServiceProviderFactory());

            //Autofac������AOP������
            builder.ConfigureContainer<ContainerBuilder>(containerBuilder =>
            {
                containerBuilder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).PropertiesAutowired();
            });

            return builder;
        }
    }
}
