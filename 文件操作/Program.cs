﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace 文件操作
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //模拟数据
            //string[] names = {"张三", "李四", "王五", "赵六" };

            string str = "2023-4-21";

            DateTime dt = str.ConvertToDatetime(90);

            

            Console.WriteLine(dt.ToString());

            Console.ReadLine();
        }
    }

    public static class DtHelper
    {
        public static DateTime ConvertToDatetime(this string str,int pre)
        {
            return Convert.ToDateTime(str).AddDays(pre);
        }
    }
}
