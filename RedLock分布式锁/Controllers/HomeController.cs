﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RedLockNet;

namespace RedLock分布式锁.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly ProductService productService;
        private readonly IDistributedLockFactory distributedLockFactory;

        public HomeController(ProductService productService, IDistributedLockFactory distributedLockFactory)
        {
            this.productService = productService;
            this.distributedLockFactory = distributedLockFactory;
        }

        [HttpGet]
        public async Task<IActionResult> BuyAsync()
        {
            var productId = "id";
            // resource 锁定的对象
            // expiryTime 锁定过期时间，锁区域内的逻辑执行如果超过过期时间，锁将被释放
            // waitTime 等待时间,相同的 resource 如果当前的锁被其他线程占用,最多等待时间
            // retryTime 等待时间内，多久尝试获取一次
            TimeSpan expiryTime = TimeSpan.FromSeconds(2);
            TimeSpan waitTime = TimeSpan.FromSeconds(3);
            TimeSpan retryTime = TimeSpan.FromMilliseconds(20);
            using (var redLock = await distributedLockFactory.CreateLockAsync(productId, expiryTime, waitTime, retryTime))
            {
                if (redLock.IsAcquired)
                {
                    var result = await productService.BuyAsync();
                    return Ok(new
                    {
                        result = result.Item1,
                        stockCount = result.Item2
                    });
                }
                else
                {
                    return Ok(new
                    {
                        result = false,
                        msg = "获取锁失败"
                    });
                }
            }
        }
    }
}
