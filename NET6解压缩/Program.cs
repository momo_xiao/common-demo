﻿// See https://aka.ms/new-console-template for more information
using System.IO.Compression;
using System.Text.RegularExpressions;

var fileName = Path.GetFullPath("../../../96332e43f84b44ffa43e004daad045d2.zip");
using (var zipToOpen = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite, FileShare.Read))
{
    using (var archive = new ZipArchive(zipToOpen, ZipArchiveMode.Read))
    {
        var count = archive.Entries.Count;
        for (int i = 0; i < count; i++)
        {
            var entries = archive.Entries[i];
            if (!entries.FullName.EndsWith("/"))
            {
                var entryFilePath = Regex.Replace(entries.FullName.Replace("/", @"\"), @"^\\*", "");
                string filePath = Path.GetFullPath($"../../../{entryFilePath}");
                var content = new byte[entries.Length];
                using (var entryStream = entries.Open())
                using (StreamReader reader = new StreamReader(entryStream))
                {
                    var jsonstr = await reader.ReadToEndAsync();
                }
            }
        }
    }
}
Console.ReadLine();