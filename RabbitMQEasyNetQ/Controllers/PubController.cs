﻿using EasyNetQ;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RabbitMQ.Client;
using RabbitMQEasyNetQ.Model;
using System.Text;

namespace RabbitMQEasyNetQ.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class PubController : ControllerBase
    {
        private readonly IModel channel;
        private readonly IBus bus;

        public PubController(IModel channel, IBus bus)
        {
            this.channel = channel;
            this.bus = bus;
        }

        [HttpGet]
        public IActionResult PublishMessage(string message)
        {
            string queueName = "my-queue.test";

            Console.WriteLine("连接成功，请输入消息。。。");

            //1、关于交换机和队列的autodelete属性
            //交换机的autodelete属性作用：当属性值设置为true,那么当所有的队列断开连接于交换机的绑定,那么交换机会自动删除
            //队列的autodelete属性作用：自动删除队列和普通队列在使用上没有什么区别，唯一的区别是，当所有的相关消费者（最后一个连接断开）断开连接时，队列将会被删除
            //2、关于队列的exclusive属性
            //如果该参数为true，则该队列仅允许创建它的连接进行写入或读取，同时当该链接关闭时，该队列被删除。
            channel.QueueDeclare(queue: queueName, durable: false, exclusive: false, autoDelete: false);

            byte[] body = Encoding.UTF8.GetBytes(message);

            channel.BasicPublish(exchange: "", routingKey: queueName, basicProperties: null, body);

            return Ok();
        }


        [HttpPost]
        public async Task<IActionResult> PublishEasyNetQMessage(TextMessage message)
        {
            await bus.PubSub.PublishAsync<TextMessage>(message);

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> PublishEasyNetQMessageDto(MessageDto message)
        {
            await bus.PubSub.PublishAsync<MessageDto>(message);

            return Ok();
        }
    }
}
