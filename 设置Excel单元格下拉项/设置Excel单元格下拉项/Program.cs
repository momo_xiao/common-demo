﻿using OfficeOpenXml;

namespace 设置Excel单元格下拉项
{
    internal class Program
    {
        static void Main()
        {
            // 创建Excel文件
            var excelFile = new FileInfo(Path.GetFullPath("../../../sample.xlsx"));

            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;

            using (var package = new ExcelPackage(excelFile))
            {
                // 创建工作表
                var worksheet = package.Workbook.Worksheets.Add("Sheet1");

                // 创建下拉列表选项
                var dropdownOptions = new List<string>
            {
                "Option 1",
                "Option 2",
                "Option 3"
            };

                // 设置下拉列表的单元格范围
                var dropdownRange = worksheet.Cells["A1:A10"];

                // 添加下拉列表选项
                var dataValidation = dropdownRange.DataValidation.AddListDataValidation();

                foreach (var item in dropdownOptions)
                {
                    dataValidation.Formula.Values.Add(item);
                }

                // 保存Excel文件
                package.Save();
            }

            Console.WriteLine("Excel文件已创建。");

            Console.ReadLine();
        }
    }
}