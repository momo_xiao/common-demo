using Application;
using AutoMapper;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

//向程序集注册

//写法一
//builder.Services.AddAutoMapper(Assembly.Load("Application"), Assembly.Load("Domain"));

//写法二
/*builder.Services.AddAutoMapper(cfg => {
    cfg.AddProfile<DemoProfile>();
});*/

//写法三
AutoMapper.IConfigurationProvider config = new MapperConfiguration(cfg =>
{
    cfg.AddProfile<DemoProfile>();
    cfg.SourceMemberNamingConvention = new PascalCaseNamingConvention();
    cfg.DestinationMemberNamingConvention = new LowerUnderscoreNamingConvention();
});

builder.Services.AddSingleton(config);

//如果已经集成Autofac，则不需此行
builder.Services.AddScoped<IMapper, Mapper>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
