﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 多线程进度条
{
    public partial class Form3 : Form
    {
        Thread thread;
        public Form3()
        {
            InitializeComponent();
        }

        public object o()
        {
            try
            {
                return new
                {
                    code = 200,
                    msg = "成功"
                };
            }
            catch (Exception e)
            {
                return new
                {
                    code = 400,
                    msg = e.Message
                };
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            thread = new Thread(obj => {
                HttpClient httpClient = new HttpClient();
                string html = httpClient.GetStringAsync(obj.ToString()).Result;
                this.Invoke(new Action(() => {
                    txtHtml.Text = html;
                }));

                Thread.Sleep(TimeSpan.FromSeconds(10));

            });

            thread.Name = "http线程";

            var url = txtUrl.Text;

            thread.IsBackground = true;

            thread.Start(url);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("这是一个消息", "标题栏文字", MessageBoxButtons.OK);
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            txtUrl.Text = "https://www.bwie.net/";
        }
    }
}
