﻿using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;

namespace NPOI导出示例
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var path1 = Path.GetFullPath("../../../Excel");
            //创建文件夹
            if (!Directory.Exists(path1))
                Directory.CreateDirectory(path1);

            //NPOI
            HSSFWorkbook workbook = new HSSFWorkbook();

            for (int i = 1; i < 10; i++)
            {
                ISheet sheet = workbook.CreateSheet($"Sheet{i}");
                if (i == 1)
                {
                    //标题行
                    IRow HeaderRow = sheet.CreateRow(0);

                    ICell HeadrCell = HeaderRow.CreateCell(0);

                    ICellStyle HeadrStyle = workbook.CreateCellStyle();

                    //单元格对齐方式
                    HeadrStyle.Alignment = HorizontalAlignment.Center;
                    HeadrStyle.VerticalAlignment = VerticalAlignment.Top;

                    //字体设置
                    IFont font = workbook.CreateFont();
                    font.FontName = "微软雅黑";
                    font.IsBold = true; //加粗
                    font.FontHeightInPoints = 20;
                    font.Color = HSSFColor.Red.Index;
                    HeadrStyle.SetFont(font);

                    HeadrStyle.BorderTop = BorderStyle.Thin;
                    HeadrStyle.BorderBottom = BorderStyle.Thin;
                    HeadrStyle.BorderLeft = BorderStyle.Thin;
                    HeadrStyle.BorderRight = BorderStyle.Thin;
                    HeadrStyle.TopBorderColor = HSSFColor.Red.Index;
                    HeadrStyle.BottomBorderColor = HSSFColor.Red.Index;
                    HeadrStyle.LeftBorderColor = HSSFColor.SkyBlue.Index;
                    HeadrStyle.RightBorderColor = HSSFColor.SkyBlue.Index;

                    var range = new NPOI.SS.Util.CellRangeAddress(0, 0, 0, 9);
                    ((HSSFSheet)sheet).SetEnclosedBorderOfRegion(range, BorderStyle.Thin, HSSFColor.Red.Index);

                    sheet.AddMergedRegion(range);
                    HeadrCell.CellStyle = HeadrStyle;
                    HeadrCell.SetCellValue("导出数据报表");

                    //行
                    for (int l = 1; l < 20; l++)
                    {
                        IRow row = sheet.CreateRow(l);
                        row.HeightInPoints = 200 * 3;
                        for (int c = 0; c < 10; c++)
                        {
                            Random random = new Random();
                            ICell cell = row.CreateCell(c);
                            ICellStyle cellStyle = workbook.CreateCellStyle();

                            cellStyle.BorderTop = BorderStyle.Thin;
                            cellStyle.BorderBottom = BorderStyle.Thin;
                            cellStyle.BorderLeft = BorderStyle.Thin;
                            cellStyle.BorderRight = BorderStyle.Thin;
                            cellStyle.TopBorderColor = HSSFColor.Red.Index;
                            cellStyle.BottomBorderColor = HSSFColor.Red.Index;
                            cellStyle.LeftBorderColor = HSSFColor.SkyBlue.Index;
                            cellStyle.RightBorderColor = HSSFColor.SkyBlue.Index;


                            IDataFormat format = workbook.CreateDataFormat();
                            cellStyle.DataFormat = format.GetFormat("yyyy年m月d日");
                            cell.CellStyle = cellStyle;

                            sheet.SetColumnWidth(c, 30 * 256);

                            //合并单元格
                            cell.SetCellValue(DateTime.Now.AddMinutes(random.Next(200, 6000)));

                            /*
                            var patr = sheet.CreateDrawingPatriarch();
                            var comment1 = patr.CreateCellComment(patr.CreateAnchor(0, 0, 0, 0, 2, 3, 4, 4));
                            comment1.String = new HSSFRichTextString("Hello World");
                            comment1.Author = "NPOI Team";
                            cell.CellComment = comment1;
                            */
                        }
                    }
                }
            }

            using (FileStream file = new FileStream($"{path1}/{DateTime.Now.ToString("yyyy-MM-dd")}.xlsx", FileMode.Create))
            {
                workbook.Write(file);
            }

            Console.WriteLine("创建完成..");
            Console.ReadLine();
        }
    }
}
