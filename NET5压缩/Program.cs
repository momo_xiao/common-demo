﻿using System;
using System.IO.Compression;
using System.IO;
using Utils.Zip;

namespace NET5压缩
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region 压缩整个目录
            ZipHelper zipHelper = new ZipHelper();
            var result = zipHelper.CreatZip(Path.GetDirectoryName($"{Path.GetFullPath("../../../")}/appsettings.json"), $"{Path.GetFullPath("../../../")}/appsettings.zip");
            #endregion

            #region 压缩单个文件            
            using (FileStream fs = new FileStream($"{Path.GetFullPath("../../../")}/appsettings.zip", FileMode.Create))
            {
                using (ZipArchive arch = new ZipArchive(fs, ZipArchiveMode.Create))
                {
                    arch.CreateEntryFromFile($"{Path.GetFullPath("../../../")}/appsettings.json", "appsettings.json");
                }
            }
            #endregion
        }
    }
}
