﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;

namespace 常见控制台面试题
{
    internal class Program
    {
        private static readonly List<Person> m_persons = new List<Person>();
        static void Main(string[] args)
        {
            var actions = new List<Action>();
            for (int i = 0; i < 5; i++)
            {
                Action action = () => { Console.WriteLine(i.ToString(CultureInfo.InvariantCulture)); };
                actions.Add(action);
            }

            foreach (var action in actions)
            {
                action();
            }

            Console.ReadLine();






            AddPerson();
            var person = new Person { Name = "AAA", Age = 17 };
            Console.WriteLine(m_persons.Contains(person));
            Console.ReadLine();
        }
        private static void AddPerson()
        {
            var person = new Person { Name = "AAA", Age = 17 };
            m_persons.Add(person);
            Console.WriteLine(m_persons.Contains(person));
        }

        public class Person
        {
            public string Name { get; set; }
            public int Age { get; set; }
        }
    }
}
