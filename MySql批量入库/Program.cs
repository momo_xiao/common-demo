﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace MySql批量入库
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            // ********************************
            // ********************************
            // 需要打开MySql的批量插入功能
            // 1、SHOW VARIABLES LIKE '%local%'，得到  local_infile   OFF  即该变量未开启
            // 2、SET GLOBAL local_infile=1，得到  local_infile   ON  即可
            // ********************************
            // ********************************

            Program program = new Program();
            //第一种调用
            var r = program.MySqlBulkCopy(program.GetData(), "members");

            //第二种调用
            r = await program.MySqlBulCopyAsync(program.GetData());
            Console.WriteLine(r);

            Console.ReadLine();
        }

        #region 第一种方式，有事务支持
        /// <summary>
        /// 一、构建模拟数据存放于DataTable
        /// </summary>
        /// <returns>DataTable</returns>
        public DataTable GetData()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("MemberID", typeof(string));//为新的Datatable添加一个新的列名
            dt.Columns.Add("Body", typeof(string));//为新的Datatable添加一个新的列名
            dt.Columns.Add("IsSecret", typeof(int));//为新的Datatable添加一个新的列名
            dt.Columns.Add("AdminReply", typeof(string));//为新的Datatable添加一个新的列名
            dt.Columns.Add("AdminReplyTime", typeof(DateTime));
            dt.Columns.Add("CreateTime", typeof(DateTime));

            for (int i = 0; i < 200000; i++) //开始循环赋值
            {
                DataRow row = dt.NewRow(); //创建一个行
                row["MemberID"] = Guid.NewGuid().ToString(); //从总的Datatable中读取行数据赋值给新的Datatable
                row["Body"] = "留言了:" + (i + 1).ToString();
                row["IsSecret"] = 0;
                row["AdminReply"] = "回复了:" + (i + 1).ToString();
                row["AdminReplyTime"] = DateTime.Now;
                row["CreateTime"] = DateTime.Now.AddMonths(-4);
                dt.Rows.Add(row);//添加次行
            }
            return dt;
        }
        /// <summary>
        /// 二、实例的数据源中的列与该实例的目标表中的列之间的映射
        /// </summary>
        /// <returns></returns>
        public MySqlBulkCopyColumnMapping[] GetMapping()
        {
            MySqlBulkCopyColumnMapping[] mapping = new MySqlBulkCopyColumnMapping[6];

            mapping[0] = new MySqlBulkCopyColumnMapping(0, "MemberID");
            mapping[1] = new MySqlBulkCopyColumnMapping(1, "Body");
            mapping[2] = new MySqlBulkCopyColumnMapping(2, "IsSecret");
            mapping[3] = new MySqlBulkCopyColumnMapping(3, "AdminReply");
            mapping[4] = new MySqlBulkCopyColumnMapping(4, "AdminReplyTime");
            mapping[5] = new MySqlBulkCopyColumnMapping(5, "CreateTime");

            return mapping;
        }
        /// <summary>
        /// DataTable批量添加(有事务)
        /// </summary>
        /// <param name="Table">数据源DataTable</param>
        /// <param name="DestinationTableName">目标表即需要插入数据的数据表名称如"Message"</param>
        public bool MySqlBulkCopy(DataTable Table, string DestinationTableName)
        {
            bool Bool = true;
            using (MySqlConnection con = new MySqlConnection("Server=localhost;Database=ttt;Uid=root;Pwd=lp_lucky;AllowLoadLocalInfile=true"))
            {
                con.Open();
                using (MySqlTransaction Tran = con.BeginTransaction())//应用事物
                {
                    //using (MySqlBulkCopy Copy = new SqlBulkCopy(con, SqlBulkCopyOptions.KeepIdentity, Tran))
                    MySqlBulkCopy Copy = new MySqlBulkCopy(con, Tran);
                    Copy.DestinationTableName = DestinationTableName;//指定目标表
                    MySqlBulkCopyColumnMapping[] Mapping = GetMapping(); ;//获取映射关系
                    if (Mapping != null)
                    {
                        //如果有数据
                        foreach (MySqlBulkCopyColumnMapping Map in Mapping)
                        {
                            Copy.ColumnMappings.Add(Map);
                        }
                    }
                    try
                    {
                        Copy.WriteToServer(Table);//批量添加
                        Tran.Commit();//提交事务
                    }
                    catch (Exception ex)
                    {
                        Tran.Rollback();//回滚事务
                        Bool = false;
                    }
                }
            }

            return Bool;
        }
        #endregion


        #region 第二种方式，无事务支持，但GetMySqlColumnMapping是自动的
        public async Task<bool> MySqlBulCopyAsync(DataTable dataTable)
        {
            try
            {
                bool result = true;
                using (var connection = new MySqlConnection("Server=localhost;Database=ttt;Uid=root;Pwd=lp_lucky;AllowLoadLocalInfile=true"))
                {
                    await connection.OpenAsync();
                    var bulkCopy = new MySqlBulkCopy(connection);
                    bulkCopy.DestinationTableName = "members";
                    // the column mapping is required if you have a identity column in the table
                    bulkCopy.ColumnMappings.AddRange(GetMySqlColumnMapping(dataTable));
                    await bulkCopy.WriteToServerAsync(dataTable);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        private List<MySqlBulkCopyColumnMapping> GetMySqlColumnMapping(DataTable dataTable)
        {
            List<MySqlBulkCopyColumnMapping> colMappings = new List<MySqlBulkCopyColumnMapping>();
            int i = 0;
            foreach (DataColumn col in dataTable.Columns)
            {
                colMappings.Add(new MySqlBulkCopyColumnMapping(i, col.ColumnName));
                i++;
            }
            return colMappings;
        }
        #endregion
    }
}
