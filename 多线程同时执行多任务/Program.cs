﻿//计时
/*Stopwatch sw = Stopwatch.StartNew();
sw.Start();

int taskCount = 1000;
for (int i = 0; i <= taskCount; i++)
{
    Thread.Sleep(100);
    Console.WriteLine("当前任务执行完成:" + i);
}

sw.Stop();
Console.WriteLine(sw.ElapsedMilliseconds + "毫秒");

Console.ReadLine();*/

using System.Diagnostics;

internal class Program
{
    private static void Main(string[] args)
    {
        //ThreadPoolDemo();
        //ThreadDemo();
        TaskDemo().Wait();
    }

    #region 线程池线程    
    /// <summary>
    /// 线程池线程
    /// </summary>
    private static void ThreadPoolDemo()
    {
        Stopwatch stopwatch = Stopwatch.StartNew();
        stopwatch.Start();
        // 定义任务数量和线程池大小
        int taskCount = 1000;
        int threadCount = 20;

        // 创建一个信号量用于控制并发线程数
        SemaphoreSlim semaphore = new SemaphoreSlim(threadCount);

        // 创建线程池
        ThreadPool.SetMaxThreads(threadCount, threadCount);

        for (int i = 0; i < taskCount; i++)
        {
            // 等待信号量可用
            semaphore.Wait();

            // 提交任务到线程池
            ThreadPool.QueueUserWorkItem(ProcessData, semaphore);
        }

        // 等待所有任务完成
        semaphore.Wait(threadCount);

        stopwatch.Stop();

        Console.WriteLine("所有任务已完成，总耗时：" + stopwatch.ElapsedMilliseconds / 1000);
    }
    #endregion

    #region 手动创建线程
    /// <summary>
    /// 手动创建线程
    /// </summary>
    private static void ThreadDemo()
    {
        Stopwatch stopwatch = Stopwatch.StartNew();
        stopwatch.Start();
        // 定义任务数量和线程数量
        int taskCount = 1000;
        int threadCount = 20;

        // 创建一个信号量用于控制并发线程数
        SemaphoreSlim semaphore = new SemaphoreSlim(threadCount);

        for (int i = 0; i < taskCount; i++)
        {
            // 等待信号量可用
            semaphore.Wait();

            // 创建线程并启动
            Thread thread = new Thread(() => ProcessData(semaphore));
            thread.Start();
        }

        // 等待所有任务完成
        while (semaphore.CurrentCount != threadCount)
        {
            Thread.Sleep(100);
        }

        stopwatch.Stop();

        Console.WriteLine("所有任务已完成，总耗时：" + stopwatch.ElapsedMilliseconds / 1000);
    }
    #endregion

    private static void ProcessData(object? state)
    {
        try
        {
            // 模拟处理数据的耗时操作
            Thread.Sleep(100);

            Console.WriteLine($"任务处理完成，线程ID: {Thread.CurrentThread.ManagedThreadId}");
        }
        finally
        {
            // 释放信号量
            ((SemaphoreSlim)state).Release();
        }
    }

    #region 通过异步方法实现
    /// <summary>
    /// 通过异步方法实现
    /// </summary>
    /// <returns></returns>
    private static async Task TaskDemo()
    {
        Stopwatch stopwatch = Stopwatch.StartNew();
        stopwatch.Start();
        // 定义任务数量和线程数量
        int taskCount = 1000;
        int threadCount = 20;

        // 创建一个信号量用于控制并发线程数
        SemaphoreSlim semaphore = new SemaphoreSlim(threadCount);

        Task[] tasks = new Task[taskCount];

        for (int i = 0; i < taskCount; i++)
        {
            // 等待信号量可用
            await semaphore.WaitAsync();

            // 创建任务并添加到数组
            tasks[i] = ProcessDataTask(semaphore);
        }

        // 等待所有任务完成
        await Task.WhenAll(tasks);

        stopwatch.Stop();

        Console.WriteLine("所有任务已完成，总耗时：" + stopwatch.ElapsedMilliseconds / 1000);
    }
    #endregion

    private static async Task ProcessDataTask(object state)
    {
        try
        {
            // 模拟处理数据的耗时操作
            await Task.Delay(100);

            Console.WriteLine($"任务处理完成，线程ID: {Thread.CurrentThread.ManagedThreadId}");
        }
        finally
        {
            // 释放信号量
            ((SemaphoreSlim)state).Release();
        }
    }
}