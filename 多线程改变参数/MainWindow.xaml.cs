﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace 多线程改变参数
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        List<@class> classes = new List<@class> { 
            new @class{ classId = 1, className = "早班", range = "00:00:00-7:29:59"  },
            new @class{ classId = 2, className = "中班", range = "7:30:00-19:29:59"  },
            new @class{ classId = 3, className = "晚班", range = "19:30:00-23:59:59"  }
        };

        private DateTime workTime,trigger;
        private bool isChange;

        public MainWindow()
        {
            InitializeComponent();

            classes.Insert(0, new @class { classId = 0, className = "请选择" });
            com_class.ItemsSource = classes;
            com_class.DisplayMemberPath = "className";
            com_class.SelectedValuePath = "classId";
            com_class.SelectedIndex = 0;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ThreadPool.QueueUserWorkItem(o => {
                var CurrClassName = string.Empty;
                var CurrClassId = 0;
                while (true)
                {
                    if (!isChange)
                    {                        
                        workTime = DateTime.Now;
                    }
                    else
                    {
                        if (DateTime.Now > trigger)
                        {
                            isChange = false;
                        }
                    }
                    
                    var currClass = classes.Skip(1).FirstOrDefault(m => {
                        var range = m.range.Split('-');
                        return
                        DateTime.Compare(Convert.ToDateTime(workTime), Convert.ToDateTime(range[0])) >= 0 &&
                        DateTime.Compare(Convert.ToDateTime(workTime), Convert.ToDateTime(range[1])) <= 0;
                    });

                    CurrClassName = currClass.className;
                    CurrClassId = currClass.classId;

                    Console.WriteLine($"当前时间:{workTime},当前班次名称:{CurrClassName},当前班次id:{CurrClassId}");

                    Thread.Sleep(TimeSpan.FromSeconds(1));
                }
            });
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            isChange = true;
            workTime = Convert.ToDateTime(classes.FirstOrDefault(m => m.classId == (int)com_class.SelectedValue).range.Split('-')[0]);

            var currClass = classes.Skip(1).FirstOrDefault(m => {
                var range = m.range.Split('-');
                return
                DateTime.Compare(DateTime.Now, Convert.ToDateTime(range[0])) >= 0 &&
                DateTime.Compare(DateTime.Now, Convert.ToDateTime(range[1])) <= 0;
            });

            trigger = Convert.ToDateTime(classes.FirstOrDefault(m => m.classId == currClass.classId + 1).range.Split('-')[0]);
        }
    }

    public class @class
    {
        public int classId { get; set; }
        public string className { get; set; }
        public string range { get; set; }
    }
}
