﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 多线程进度条
{
    public partial class Form1 : Form
    {
        Thread thread;

        bool IsPause = false;

        public Form1()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
            thread = new Thread(() => {
                for (int i = 0; i <= 100; i++)
                {
                    this.progressBar1.Value = i;

                    if (IsPause) 
                    { 
                        Thread.Sleep(TimeSpan.FromSeconds(9));
                        IsPause = false;
                    }

                    Thread.Sleep(TimeSpan.FromMilliseconds(50));
                }
            });
            thread.IsBackground = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if(thread.ThreadState == (ThreadState.Unstarted | ThreadState.Background))
                thread.Start();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            IsPause = true;
        }

        /// <summary>
        /// 挂起
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [Obsolete]
        private void button4_Click_1(object sender, EventArgs e)
        {
            thread.Suspend();
        }

        [Obsolete]
        private void button5_Click_1(object sender, EventArgs e)
        {
            thread.Resume();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            if(thread.ThreadState == ThreadState.Running)
            {
                thread.Abort();
            }            
        }
    }
}
