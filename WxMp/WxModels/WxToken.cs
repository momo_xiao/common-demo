﻿namespace WxMp.WxModels
{
    public class WxToken
    {
        public string access_token { get; set; }
        public string expires_in { get; set; }
    }
}
