﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 文件操作
{
    using System;
    using System.IO;

    class FileHelper
    {
        // 创建文件夹
        public static void CreateDirectory(string path)
        {
            try
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                    Console.WriteLine("创建文件夹成功：" + path);
                }
                else
                {
                    Console.WriteLine("该文件夹已存在：" + path);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("创建文件夹失败：" + ex.Message);
            }
        }

        // 删除文件夹
        public static void DeleteDirectory(string path)
        {
            try
            {
                if (Directory.Exists(path))
                {
                    Directory.Delete(path, true); // 设置为 true，表示删除该文件夹下的所有文件和文件夹
                    Console.WriteLine("删除文件夹成功：" + path);
                }
                else
                {
                    Console.WriteLine("该文件夹不存在：" + path);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("删除文件夹失败：" + ex.Message);
            }
        }

        // 创建文件
        public static void CreateFile(string path)
        {
            try
            {
                if (!File.Exists(path))
                {
                    File.Create(path).Close();
                    Console.WriteLine("创建文件成功：" + path);
                }
                else
                {
                    Console.WriteLine("该文件已存在：" + path);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("创建文件失败：" + ex.Message);
            }
        }

        // 写入文件
        public static void WriteFile(string path, string content)
        {
            try
            {
                File.WriteAllText(path, content);
                Console.WriteLine("写入文件成功：" + path);
            }
            catch (Exception ex)
            {
                Console.WriteLine("写入文件失败：" + ex.Message);
            }
        }

        // 读取文件
        public static string ReadFile(string path)
        {
            try
            {
                string content = File.ReadAllText(path);
                Console.WriteLine("读取文件成功：" + path);
                return content;
            }
            catch (Exception ex)
            {
                Console.WriteLine("读取文件失败：" + ex.Message);
                return "";
            }
        }

        // 删除文件
        public static void DeleteFile(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                    Console.WriteLine("删除文件成功：" + path);
                }
                else
                {
                    Console.WriteLine("该文件不存在：" + path);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("删除文件失败：" + ex.Message);
            }
        }
    }
}
