﻿using CSRedis;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Text;

namespace WebSocketDemo.Controllers
{
    public class HomeController : Controller
    {
        private readonly CSRedisClient cSRedisClient;
        private readonly Dictionary<string, WebSocket> websockets;
        private byte[] buffer = new byte[1024 * 4];

        public HomeController(CSRedisClient cSRedisClient, Dictionary<string, WebSocket> websockets)
        {
            this.cSRedisClient = cSRedisClient;
            this.websockets = websockets;
        }

        public IActionResult Index()
        {
            return View();
        }

        #region MyRegion
        /*[HttpGet]
        public async Task<IActionResult> AddUser(string UserName)
        {
            const string chatuser = "char:user";

            if (!await cSRedisClient.HExistsAsync(chatuser, UserName))
            {
                await cSRedisClient.HSetAsync(chatuser, UserName, string.Empty);
            }

            return Ok(await cSRedisClient.HGetAllAsync(chatuser));
        }*/
        #endregion


        [HttpGet("/ws")]
        public async Task Get()
        {
            if (HttpContext.WebSockets.IsWebSocketRequest)
            {
                using var webSocket = await HttpContext.WebSockets.AcceptWebSocketAsync();

                //接收数据
                var receiveResult = await webSocket.ReceiveAsync(
                    new ArraySegment<byte>(buffer), CancellationToken.None);

                //从缓冲区中按接收到的数量，获取接收到的数据
                var json = Encoding.UTF8.GetString(buffer, 0, receiveResult.Count);

                websockets.Add(json, webSocket);

                //向客户端发送好友列表
                foreach (var item in websockets)
                {
                    if (item.Value != null)
                    {
                        var users = JsonConvert.SerializeObject(new
                        {
                            users = websockets.Keys.ToArray(),
                        });
                        await item.Value.SendAsync(
                            new ArraySegment<byte>(Encoding.UTF8.GetBytes(users)),
                            WebSocketMessageType.Text,
                            true,
                            CancellationToken.None);
                    }
                }

                await Echo(webSocket);
            }
            else
            {
                HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
            }
        }

        private async Task Echo(WebSocket webSocket)
        {
            while (webSocket.State == WebSocketState.Open)
            {
                var receiveResult = await webSocket.ReceiveAsync(
                    new ArraySegment<byte>(buffer), CancellationToken.None);

                //获取接收到的消息
                fromMessage msg = JsonConvert.DeserializeObject<fromMessage>(Encoding.UTF8.GetString(buffer, 0, receiveResult.Count));


                //群发

                var w = websockets.First(m => m.Key == "李四");

                foreach (var item in websockets)
                {
                    if (item.Value != null)
                    {

                        message message = new message
                        {
                            content = msg.msg,
                            datetime = DateTime.Now,
                            userName = msg.from
                        };

                        await item.Value.SendAsync(
                            new ArraySegment<byte>(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message))),
                            WebSocketMessageType.Text,
                            true,
                            CancellationToken.None);
                    }
                }
            }
        }
    }

    public class message
    {
        public string userName { get; set; }
        public string content { get; set; }
        public DateTime datetime { get; set; }
    }

    public class fromMessage
    {
        public string from { get; set; }
        public string msg { get; set; }
    }
}
