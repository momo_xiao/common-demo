﻿using StackExchange.Redis;

namespace StackExchange实现分布式锁
{
    /// <summary>
    /// redis分布式锁
    /// 1、封装redis分布锁
    ///    1、加锁
    ///    2、解锁
    /// 2、应用分布式锁
    /// </summary>
    public class RedisLock
    {
        private readonly ILogger<RedisLock> logger;

        // 1、redis连接管理类
        private ConnectionMultiplexer connectionMultiplexer = null;

        // 2、redis数据操作类
        private IDatabase database = null;

        private string token = Guid.NewGuid().ToString("N");

        public RedisLock(ILogger<RedisLock> logger)
        {
            connectionMultiplexer = ConnectionMultiplexer.Connect("127.0.0.1:6379,password=feeling");

            database = connectionMultiplexer.GetDatabase(0);
            this.logger = logger;
        }

        /// <summary>
        /// 加锁
        /// 1、key:锁名称
        /// 2、value:谁加的这把锁。线程1
        /// 3、exprie：过期时间：目的是为了防止死锁
        /// 
        /// </summary>

        public void Lock()
        {
            while (true)
            {
                bool flag = database.LockTake("redis-lock", token, TimeSpan.FromSeconds(60));
                // 1、true 加锁成功 2、false 加锁失败
                if (flag)
                {
                    logger.LogInformation("加锁成功");
                    break;
                }
                // 防止死循环。通过等待时间，释放资源
                Thread.Sleep(10);
            }
        }

        /// <summary>
        /// 解锁
        /// </summary>

        public void UnLock()
        {
            bool flag = database.LockRelease("redis-lock", token);
            logger.LogInformation("解锁结果：" + flag);

            // true:释放成功  false 释放失败
            // 方案：释放资源
            connectionMultiplexer.Close();
        }
    }
}