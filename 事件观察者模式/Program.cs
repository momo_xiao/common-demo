﻿using System;

namespace 事件观察者模式
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //创建主题
            Subject subject = new Subject();

            //创建观察者
            Observer observer1 = new Observer("Observer 1");
            Observer observer2 = new Observer("Observer 2");
            ObserverA observer3 = new ObserverA("Observer 3");

            //订阅事件
            subject.StateChanged += observer1.OnStateChanged;
            subject.StateChanged += observer2.OnStateChanged;
            subject.StateChanged += observer3.OnStateChanged;

            //模拟主题状态改变
            subject.ChangeState("new state");

            //取消观察者1的订阅
            subject.StateChanged -= observer1.OnStateChanged;

            //再次模拟主题状态改变
            subject.ChangeState("another state");

            Console.ReadKey();
        }
    }

    //主题类，定义事件和通知方法
    /// <summary>
    /// 猫
    /// </summary>
    public class Subject
    {
        public delegate void StateChangedEventHandler(string newState);  //声明事件的委托类型
        public event StateChangedEventHandler StateChanged;  //声明事件

        public void ChangeState(string newState)
        {
            //StateChanged?.Invoke(newState);  //触发事件，通知所有观察者

            if(StateChanged != null)
            {
                StateChanged(newState);
            }
        }
    }

    //观察者类
    /// <summary>
    /// 人
    /// </summary>
    public class Observer
    {
        private string _name;

        public Observer(string name)
        {
            _name = name;
        }

        //事件处理方法
        public void OnStateChanged(string newState)
        {
            Console.WriteLine("Observer {0} has received the subject's notification. New state: {1}", _name, newState);
        }
    }

    /// <summary>
    /// 老鼠
    /// </summary>
    public class ObserverA
    {
        private string _name;

        public ObserverA(string name)
        {
            _name = name;
        }

        //事件处理方法
        public void OnStateChanged(string newState)
        {
            Console.WriteLine("Observer {0} has received the subject's notification. New state: {1}", _name, newState);
        }
    }
}
