﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    public class MemberDto
    {
        public int MemberId { get; set; }
        public string MemberName { get; set; }
        public int Age { get; set; }
        public DateTime Birthday { get; set; }
    }
}
